// pages/comment/list.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    getListParam: {
      ProductId:'',
      OffSet: 0,
      Size: 8,
      list: []
    },

    lastRequestCount: 8
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.hideShareMenu();
    this.setData({
      'getListParam.ProductId': options.id
    });
    this.initList();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.setData({
      'getListParam.OffSet': 0,
      lastRequestCount: 8
    });
    this.initList('stopPullDownRefresh');
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    this.setData({
      'getListParam.OffSet': this.data.getListParam.OffSet + 1
    });
    this.initList('onReachBottom');
  },
  initList: function (type) {
    if (this.data.lastRequestCount < this.data.getListParam.Size)
      return false;//防止到最后一页继续请求
    var that = this;
    wx.showLoading({
      title: '读取评价中',
    })
    wx.request({
      url: app.appSetting.host + "/Comment/search/data",
      data: {
        ProductId: that.data.getListParam.ProductId,
        OffSet: that.data.getListParam.OffSet,
        Size: that.data.getListParam.Size
        
      },
      method: 'POST',
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: function (res) {
        console.log(res);
        wx.hideLoading();
        if (res.statusCode == 200) {
          that.setData({
            lastRequestCount: res.data.length
          });
          if (type == 'stopPullDownRefresh') {
            that.setData({
              'getListParam.list': res.data
            });
            wx.stopPullDownRefresh()
          }
          if (type == 'onReachBottom') {
            that.setData({
              'getListParam.list': that.data.getListParam.list.concat(res.data)
            });
          } else {
            that.setData({
              'getListParam.list': res.data
            });
          }
        } else {
          wx.showToast({
            title: res.data,
            icon: 'none'
          })
        }
      }
    })

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})