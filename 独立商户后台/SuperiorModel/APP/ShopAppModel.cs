﻿using System.ComponentModel.DataAnnotations;

namespace SuperiorModel
{

    public class ShopAppModel
    {
        /// <summary>
        /// 店铺ID
        /// </summary>
        public string Id { get; set; }
        public string AppId { get; set; }
        public string AppSecret { get; set; }
        public string PaymentId { get; set; }
        public string PaySecret { get; set; }
        public string CreateTime { get; set; }
        public string UpdateTime { get; set; }
        public string ShopAdminId { get; set; }
        public string LoginName { get; set; }
        public string ServiceQQ { get; set; }
        public string ServiceWx { get; set; }
        [Required(ErrorMessage = "客服电话不能为空")]
        public string ServicePhone { get; set; }
        [Required(ErrorMessage = "店铺名称不能为空")]
        public string ShopName { get; set; }
        /// <summary>
        /// 店铺加密ID
        /// </summary>
        public string SecretId { get; set; }
        public int OptionStatus { get; set; }
        public string Remark { get; set; }
       

    }
}
