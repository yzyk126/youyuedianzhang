﻿namespace SuperiorModel
{
    public class ProductCriteria:PagedBaseModel
    {
        public int ShopAdminId { get; set; }
        public string ProductName { get; set; }
        public string Brand { get; set; }
        public string MenuId { get; set; }
        public string LoginName { get; set; }
        public int OptionStatus { get; set; }

    }

}
