﻿namespace SuperiorModel.Criteria.Marketing
{
    class MarketingCriteria
    {
    }
    public class UserCouponCriteria : PagedBaseModel
    {
        public int ShopAdminId { get; set; }
        public string NickName { get; set; }
        public int CouponType { get; set; }

    }
}
