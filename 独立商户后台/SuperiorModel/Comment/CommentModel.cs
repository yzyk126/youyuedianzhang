﻿namespace SuperiorModel
{
    public class CommentModel
    {
        public int Id { get; set; }
        public string NickName { get; set; }
        public string HeadImgUrl { get; set; }
        public string OrderNo { get; set; }
        public string LoginName { get; set; }
        public string ShopName { get; set; }
        public int CommentLevel { get; set; }
        public string CommentMsg { get; set; }
        public string ProductName { get; set; }
        public string CreateTime { get; set; }
    }
}
