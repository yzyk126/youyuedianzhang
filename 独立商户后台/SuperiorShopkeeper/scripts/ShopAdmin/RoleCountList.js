﻿(function () {
    RoleCountListClass = {};
    RoleCountListClass.Instance = {
        Init: function () {
            $(document).on('click', '#btn_addRole', this.Add);
            $(document).on('click', '.editRole', this.Edit);
            $(document).on('click', '.DeleteRole', this.Delete);
            $(document).on('click', '.OptionAccount', this.Option);

        },
        Add:function(){
            var iframeIndex = layer.open({
                type: 2,
                area: ['700px', '450px'],
                fixed: false, //不固定
                maxmin: true,
                btn: ['确认选择'],
                yes: function (index, layero) {
                    var iframeid = "layui-layer-iframe" + iframeIndex; //获取iframe分配的ID
                    //获取子页面所有勾选框
                    var contens = $("#" + iframeid + "").contents();
                    var account = $.trim(contens.find("#Account").val());
                    var password = $.trim(contens.find("#Password").val());
                    var roleId = $.trim(contens.find("#RoleId").val());
                    if (account == "" || password == "" || roleId == "") {
                        layer.alert("请检查参数是否有未写");
                        return false;
                    }
                    if (password.length < 6) {
                        layer.alert("密码长度不能小于6位");
                        return false;
                    }
                    var model = {};
                    model.Account = account;
                    model.PassWord = password;
                    model.RoleId = roleId;
                    var index = layer.load(1);
                    RequestManager.Ajax.Post("/ShopAdmin/AddOrEditRoleAccount", model
                    , true, function (data) {
                        layer.close(index);
                        if (data.IsSuccess) {
                            layer.alert("添加成功", function () {
                                layer.closeAll();
                                window.location.href = "/ShopAdmin/ShopRoleAccountList";
                            });
                            


                        } else {
                            layer.alert(data.Message);
                        }
                    })
                },
                content: '/ShopAdmin/AddOrEditAccount?id=0'
            });
        },
        Edit: function () {
            var id = $(this).attr("OptionId");
            var iframeIndex = layer.open({
                type: 2,
                area: ['700px', '450px'],
                fixed: false, //不固定
                maxmin: true,
                btn: ['确认选择'],
                yes: function (index, layero) {
                    var iframeid = "layui-layer-iframe" + iframeIndex; //获取iframe分配的ID
                    //获取子页面所有勾选框
                    var contens = $("#" + iframeid + "").contents();
                    var account = $.trim(contens.find("#Account").val());
                    var password = $.trim(contens.find("#Password").val());
                    var roleId = $.trim(contens.find("#RoleId").val());
                    if (account == "" || password == "" || roleId == "") {
                        layer.alert("请检查参数是否有未写");
                        return false;
                    }
                    if (password.length < 6) {
                        layer.alert("密码长度不能小于6位");
                        return false;
                    }
                    var model = {};
                    model.Account = account;
                    model.PassWord = password;
                    model.RoleId = roleId;
                    model.Id = id;
                    var index = layer.load(1);
                    RequestManager.Ajax.Post("/ShopAdmin/AddOrEditRoleAccount", model
                    , true, function (data) {
                        layer.close(index);
                        if (data.IsSuccess) {
                            layer.alert("修改成功", function () {
                                layer.closeAll();
                                window.location.href = "/ShopAdmin/ShopRoleAccountList";
                            });



                        } else {
                            layer.alert(data.Message);
                        }
                    })
                },
                content: '/ShopAdmin/AddOrEditAccount?id='+id
            });
        },
        Option: function () {
            var id = $(this).attr("OptionId");
            var opt = $(this).attr("Opt");
            var index = layer.load(1);
            $.get("/ShopAdmin/OptionAccount?id=" + id + "&optionStatus=" + opt + "&r=" + new Date().getTime(), function (data) {
                layer.close(index);
                if (data.IsSuccess) {
                    layer.alert("执行成功", function () {
                        window.location.href = "/ShopAdmin/ShopRoleAccountList";
                    })

                } else {
                    layer.alert(data.Message);
                }
            })
        },
     

        Delete: function () {
            var id = $(this).attr("OptionId");
            layer.confirm('删除后数据不可恢复，确定要执行吗？', {
                btn: ['确定', '取消'] //按钮
            }, function () {
                var index = layer.load(1);
                $.get("/ShopAdmin/DeleteRoleAccount?id=" + id + "&r=" + new Date().getTime(), function (data) {
                    layer.close(index);
                    if (data.IsSuccess) {
                        layer.alert("执行成功", function () {
                            window.location.href = "/ShopAdmin/ShopRoleAccountList";
                        })

                    } else {
                        layer.alert(data.Message);
                    }
                })
            }, function () {

            });
        }
        
    
    };
})();