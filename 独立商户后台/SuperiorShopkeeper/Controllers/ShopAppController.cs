﻿using SuperiorShopBussinessService;
using System;
using System.Linq;
using System.Web.Mvc;
using SuperiorModel;
using SuperiorCommon;
using Redis;
using System.IO;

namespace SuperiorShopkeeper.Controllers
{
    [JsonException]
    [CheckLoginFitler]
    public class ShopAppController : BaseController
    {
        private readonly IShopAppService _IShopAppService;
        private readonly IShopAdminService _IShopAdminService;

        public ShopAppController(IShopAppService IShopAppService, IShopAdminService IShopAdminService)
        {
            _IShopAppService = IShopAppService;
            _IShopAdminService = IShopAdminService;
        }
        protected override void Dispose(bool disposin)
        {
            this._IShopAppService.Dispose();
            this._IShopAdminService.Dispose();
            base.Dispose(disposin);
        }
        // GET: ShopApp
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 商城列表
        /// </summary>
        /// <returns></returns>
        /// 
        [ShopAuthority(AuthorityMenuEnum.ShopAppList)]
        public ActionResult List()
        {
            var criteria = new ShopAppCriteria() { PagingResult = new PagingResult(0, 15), ShopAdminId = UserContext.DeSecretId };
            var model = GetShopAppTPagedModel(criteria);
            return View(model);
        }
        /// <summary>
        /// 搜索，分页请求的商城列表
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        [AjaxOnly]
        [HttpPost]
        public ActionResult List(ShopAppCriteria criteria)
        {
            criteria.ShopAdminId = UserContext.DeSecretId;
            var model = GetShopAppTPagedModel(criteria);
            return PartialView("_List", model);
        }
 

        /// <summary>
        /// 获取商城列表分页模型
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public TPagedModelList<ShopAppModel> GetShopAppTPagedModel(ShopAppCriteria criteria)
        {
            var query = _IShopAppService.ShopAppList(criteria);
            return new TPagedModelList<ShopAppModel>(query, query.PagingResult);
        }

        /// <summary>
        /// 添加小程序页面
        /// </summary>
        /// <returns></returns>
        public ActionResult Add()
        {
            return View();
        }
        

        /// <summary>
        /// 提交商城
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [AjaxOnly]
        [HttpPost]
        [ShopAuthority(AuthorityOptionEnum.AddShopApp)]
        public JsonResult Add(ShopAppModel model)
        {
            if (!ModelState.IsValid)
                return Error(base.ErrorMsg());
            string msg = "";
            model.ShopAdminId = UserContext.DeSecretId;
            var ret = _IShopAppService.Add(model, out msg);
            if (ret != 1)
                return Error(msg);
            return Success(true);
        }

        /// <summary>
        /// 修改页面
        /// </summary>
        /// <returns></returns>
        public ActionResult Edit(string id)
        {
            string _id = base.ValidataParms(id);
            if (_id == null)
                return Error("非法参数");
            var model = _IShopAppService.GetShopAppModel(_id.ToInt());
            return View(model);
        }


        /// <summary>
        /// 提交修改
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        /// 
        [AjaxOnly]
        [HttpPost]
        [ShopAuthority(AuthorityOptionEnum.EditShopApp)]
        public JsonResult Edit(ShopAppModel model)
        {
            if (!ModelState.IsValid)
                return Error(base.ErrorMsg());
            model.Id = base.ValidataParms(model.Id);
            if (model.Id == null)
                return Error("非法参数");
            string msg = "";
            model.ShopAdminId = UserContext.DeSecretId;
            var ret = _IShopAppService.Edit(model, out msg);
            if (ret != 1)
                return Error(msg);
            //控制redis
            RedisManager.Remove("GetShopAppModel_" + model.Id);
            return Success(true);
        }

       
        /// <summary>
        /// 商城首页预览
        /// </summary>
        /// <returns></returns>
        public ActionResult Priview()
        {
            //读取当前商户的一个商城。
            var criteria = new ShopAppCriteria() { PagingResult = new PagingResult(0, 15), ShopAdminId = UserContext.DeSecretId };
            var model = GetShopAppTPagedModel(criteria);
            var shop = model.TList.FirstOrDefault();
            ViewBag.Shop = shop;
            return View();
        }
    }
}