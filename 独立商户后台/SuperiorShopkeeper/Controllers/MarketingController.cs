﻿using SuperiorCommon;
using SuperiorModel;
using SuperiorModel.Criteria.Marketing;
using SuperiorShopBussinessService;
using System.Web.Mvc;

namespace SuperiorShopkeeper.Controllers
{
    [JsonException]
    [CheckLoginFitler]
    public class MarketingController : BaseController
    {
        // GET: Marketing
        public ActionResult Index()
        {
            return View();
        }
        private readonly IMarketingService _IMarketingService;
        private readonly IProductService _IProductService;
        private readonly IAreaService _IAreaService;

        public MarketingController(IMarketingService IMarketingService, IProductService IProductService, IAreaService IAreaService)
        {
            _IMarketingService = IMarketingService;
            _IProductService = IProductService;
            _IAreaService = IAreaService;
        }
        protected override void Dispose(bool disposin)
        {
            this._IMarketingService.Dispose();
            this._IProductService.Dispose();
            this._IAreaService.Dispose();
            base.Dispose(disposin);
        }
        /// <summary>
        /// 满减设置页面
        /// </summary>
        /// <returns></returns>
        /// 
        [ShopAuthority(AuthorityMenuEnum.FullAmountSendSet)]
        public ActionResult FullAmountSendSet()
        {
            var model = _IMarketingService.GetSetModel(UserContext.DeSecretId.ToInt());
            return View(model);
        }
        /// <summary>
        /// 当前商品勾选页面
        /// </summary>
        /// <returns></returns>
        public ActionResult CurrentProducts()
        {
            var criteria = new ProductCriteria() { PagingResult = new PagingResult(0, 50), ShopAdminId = UserContext.DeSecretId.ToInt(), OptionStatus = 999 };

            var model = _IProductService.ProductList(criteria);
            var hasProductIdList = _IMarketingService.GetSetModel(UserContext.DeSecretId.ToInt()).NotHasProductIdList;
            ViewBag.HasProductIdList = hasProductIdList;
            return View(model);
        }
        /// <summary>
        /// 当前勾选省页面
        /// </summary>
        /// <returns></returns>
        public ActionResult CurrentAreas()
        {
            var model = _IAreaService.Get(0);
            var hasAreaIdList = _IMarketingService.GetSetModel(UserContext.DeSecretId.ToInt()).NotHasProvinceIdList;
            ViewBag.HasAreaIdList = hasAreaIdList;
            return View(model);
        }
        /// <summary>
        /// 设置不参与满减的商品
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        [ShopAuthority(AuthorityOptionEnum.SetNotPids)]
        public JsonResult SetNotPids(string ids)
        {
            var sid = UserContext.DeSecretId.ToInt();
            _IMarketingService.SetNotPids(ids, sid);
            return Success(true);
        }

        /// <summary>
        /// 设置不参与满减的地区
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        [ShopAuthority(AuthorityOptionEnum.SetNotAreas)]
        public JsonResult SetNotAreas(string ids)
        {
            var sid = UserContext.DeSecretId.ToInt();
            _IMarketingService.SetNotAreas(ids, sid);
            return Success(true);
        }

        /// <summary>
        /// 设置满减模型
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        [ShopAuthority(AuthorityOptionEnum.SetFullSendModel)]
        public JsonResult SetFullSendModel(FullSetModel model)
        {
            if (model.Amount < 0)
            {
                return Error("金额必须为大于等于零");
            }
            var sid = UserContext.DeSecretId.ToInt();
            _IMarketingService.SetFullAmount(model.OptionStatus, model.Amount, sid);
            return Success(true);
        }
        /// <summary>
        /// 优惠券管理列表
        /// </summary>
        /// <returns></returns>
        /// 
        [ShopAuthority(AuthorityMenuEnum.CouponList)]
        public ActionResult CouponList()
        {
            var sid = UserContext.DeSecretId.ToInt();
            var model = _IMarketingService.GetCouponList(sid);
            return View(model);
        }

        public ActionResult Add()
        {
            return View();
        }
        /// <summary>
        /// 添加优惠券
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [AjaxOnly]
        [HttpPost]
        [ShopAuthority(AuthorityOptionEnum.AddCoupon)]
        public JsonResult AddCoupon(CouponModel model)
        {
            if (!ModelState.IsValid)
                return Error(base.ErrorMsg());
            if (model.CouponType == 2)
            {
                if (!ValidateUtil.IsValidInt(model.Discount.ToString()))
                    return Error("折扣输入不合法");
                if (model.Discount < 1 || model.Discount > 100)
                    return Error("折扣输入不合法");
            }
            model.ShopAdminId = UserContext.DeSecretId.ToInt();
            _IMarketingService.AddCoupon(model);
            return Success(true);
        }
        /// <summary>
        /// 删除优惠券
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [AjaxOnly]
        [ShopAuthority(AuthorityOptionEnum.DelCoupon)]
        public JsonResult DelCoupon(int id)
        {
            _IMarketingService.DelCoupon(id);
            return Success(true);
        }
        /// <summary>
        /// 优惠券领取列表
        /// </summary>
        /// <returns></returns>
        /// 
       [ShopAuthority(AuthorityMenuEnum.UserCouponList)]
        public ActionResult UserCouponList()
        {
            var sid = UserContext.DeSecretId.ToInt();
            var criteria = new UserCouponCriteria() { ShopAdminId = sid, CouponType = 999 ,PagingResult=new PagingResult(0,30)};
            var model = GetPagedUserCouponModels(criteria);
            return View(model);
        }
        /// <summary>
        /// 优惠券领取列表搜素
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        [AjaxOnly]
        [HttpPost]
        [ShopAuthority(AuthorityOptionEnum.UserCouponList)]
        public ActionResult UserCouponList(UserCouponCriteria criteria)
        {
            criteria.ShopAdminId = UserContext.DeSecretId.ToInt();
            var model = GetPagedUserCouponModels(criteria);
            return PartialView("_UserCouponList", model);
        }

        private TPagedModelList<UserCouponModel> GetPagedUserCouponModels(UserCouponCriteria criteria)
        {
            var query = _IMarketingService.SearchUserCoupon(criteria);
            return new TPagedModelList<UserCouponModel>(query, query.PagingResult);
        }

    }
}