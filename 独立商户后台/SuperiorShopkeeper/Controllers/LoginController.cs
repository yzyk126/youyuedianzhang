﻿using System;
using System.Web.Mvc;
using SuperiorCommon;
using SuperiorModel;
using SuperiorShopBussinessService;

namespace SuperiorShopkeeper.Controllers
{
    [JsonException]
    public class LoginController : BaseController
    {
        private readonly IShopService _IShopService;

        public LoginController(IShopService IShopService)
        {
            _IShopService = IShopService;
        }
        protected override void Dispose(bool disposin)
        {
            this._IShopService.Dispose();
            base.Dispose(disposin);
        }
        // GET: Login
        public ActionResult Login()
        {
            return View();
        }

        /// <summary>
        /// 登录验证码
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        public FileContentResult CaptchaImage()
        {
            var captcha = new LiteralCaptcha(115, 48, 4);
            var bytes = captcha.Generate();
            Session["captcha"] = captcha.Captcha;
            return new FileContentResult(bytes, "image/jpeg"); 
        }
        /// <summary>
        /// 注册页面
        /// </summary>
        /// <returns></returns>
        public ActionResult Register()
        {
            return View();
        }
        /// <summary>
        /// 注册
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [AjaxOnly]
        [HttpPost]
        public JsonResult Register(ShopInfoModel model)
        {
            if (!ModelState.IsValid)
                return Error(base.ErrorMsg());
            if (!ValidateUtil.IsValidUserName(model.LoginName))
                return Error("用户名的长度（8-20个字符）");
            if (!ValidateUtil.IsValidPassword25(model.PassWord))
                return Error("密码6-25位的字母或数字组合");
            if (model.PassWord != model.ResetPassWord)
                return Error("两次密码不一致");
            //验证验证码
            var _code = Redis.RedisManager.Get<string>("register"+model.PhoneNumber + model.Code);
            if (_code != model.Code)
                return Error("请输入正确的验证码");
            string msg = string.Empty;
            int ret = _IShopService.InsertShop(model,out msg);
            if (ret != 1)
                return Error(msg);
            return Success(true);
        }

        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [AjaxOnly]
        [HttpPost]
        public JsonResult Login(ShopLoginParams model)
        {
            if (!ModelState.IsValid)
                return Error(base.ErrorMsg());
            var captcha = Session["captcha"];
            if (captcha == null)
                return Error("验证码错误");
            string strcaptcha = Convert.ToString(captcha);
            if (strcaptcha != model.Code)
                return Error("验证码错误");
            //查询商城数据
            var shop = _IShopService.GetShopModel(model.UserName,MD5Manager.MD5Encrypt(model.PassWord));
            if (string.IsNullOrEmpty(shop.Id))
                return Error("账号或密码错误");
            if (shop.OptionStatus == 2)
                return Error("账号已被冻结");
            if (shop.OptionStatus == 0)
                return Error("账号审核中");
            shop.ShopAdminType = 1;
            SessionManager.SetSession(SessionKey.ShopKey, shop);
            return Success(true);
        }

        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [AjaxOnly]
        [HttpPost]
        public JsonResult Login_Role(ShopLoginParams model)
        {
            if (!ModelState.IsValid)
                return Error(base.ErrorMsg());
            var captcha = Session["captcha"];
            if (captcha == null)
                return Error("验证码错误");
            string strcaptcha = Convert.ToString(captcha);
            if (strcaptcha != model.Code)
                return Error("验证码错误");
            //查询商城数据
            var shop = _IShopService.GetShopModel_Role(model.UserName, MD5Manager.MD5Encrypt(model.PassWord));
            if (string.IsNullOrEmpty(shop.Id))
                return Error("账号或密码错误");
            if (shop.OptionStatus == 2)
                return Error("该店主账号已被冻结");
            if (shop.OptionStatus == 0)
                return Error("账号审核中");
            if (shop.ShopLoginRoleModel.OptionStatus == -1)
                return Error("当前员工账号被冻结");
            SessionManager.SetSession(SessionKey.ShopKey, shop);
            return Success(true);
        }
        /// <summary>
        /// 退出
        /// </summary>
        /// <returns></returns>
        [AjaxOnly]
        [HttpGet]
        public ActionResult Exist()
        {
            this.Session.Clear();
            return Success(true);
        }

        /// <summary>
        /// 获取短信验证码
        /// </summary>
        /// <returns></returns>
        [AjaxOnly]
        public JsonResult GetCode(string phone)
        {
            if (!ValidateUtil.IsValidMobile(phone))
                return Error("请输入正确得手机号");
            //var code = new Random().Next(100000, 999999).ToString();
            var code = "111111";//测试放开
            Redis.RedisManager.Set<string>("register" + phone + code, code, DateTime.Now.AddMinutes(5));
            //var res = Aly.Sms.SmsManager.Instance.Send(phone, code);
            //if (res != 200)
            //    return Error("发送失败");
            return Success(true);
        }

    }
}