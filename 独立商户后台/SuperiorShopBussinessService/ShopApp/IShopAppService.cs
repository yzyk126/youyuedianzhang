﻿using SuperiorModel;

namespace SuperiorShopBussinessService
{
    public interface IShopAppService:IService
    {
        PagedSqlList<ShopAppModel> ShopAppList(ShopAppCriteria criteria);
        int Add(ShopAppModel model, out string msg);
        ShopAppModel GetShopAppModel(int id);
        int Edit(ShopAppModel model, out string msg);
        string GetPayKeyByAppid(string appid);
        void OptionShopAppStatus(int id, int option);
    }
}
