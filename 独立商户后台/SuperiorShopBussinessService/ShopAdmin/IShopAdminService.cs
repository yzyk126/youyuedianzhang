﻿using SuperiorModel;
using System.Collections.Generic;

namespace SuperiorShopBussinessService
{
    public interface IShopAdminService:IService
    {
        ShopInfoModel GetShopAdminModel(int id);
        AdminInfoModel GetAdminModel(string loginName, string passWord);
        void EditShopAdmin(ShopInfoModel model);
        void EditPassWord(int shopAdminId, string pw);
        List<ShopRoleModel> GetRoleList(int shopAdminId);
        int AddShopRole(ShopRoleModel model, out string msg);
        int EditShopRole(ShopRoleModel model, out string msg);
        int DelShopRole(int id, out string msg);
        ShopRoleModel GetRole(int id);
        void SetShopRoleAuthority(ShopRoleModel model);

        void DelShopRoleAccount(int id);
        void OptionShopRoleAccount(int id, int option);
        int EditShopRoleAccount(ShopRoleAccountModel model, out string msg);
        int AddShopRoleAccount(ShopRoleAccountModel model, out string msg);
        ShopRoleAccountModel GetRoleAccountDs(int id);
        List<ShopRoleAccountModel> GetRoleAccountListDs(int shopAdminId);

       
    }
}
