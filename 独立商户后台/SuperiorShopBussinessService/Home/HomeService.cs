﻿using SuperiorModel;
using SuperiorShopDataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace SuperiorShopBussinessService
{
    public class HomeService : IHomeService
    {
        public void Dispose()
        {

        }

        public HomeModel GetHomeData(int shopAdminId)
        {
            var res = new HomeModel();
            var ds = HomeDataAccess.GetHomeData(shopAdminId);
            if (DataManager.CheckDs(ds, 6))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    res.TotalProductCount = DataManager.GetRowValue_Int(ds.Tables[0].Rows[0]["TotalProductCount"]);
                }
                if (DataManager.CheckHasRow(ds.Tables[1]))
                {
                    res.TotalUserCount = DataManager.GetRowValue_Int(ds.Tables[1].Rows[0]["TotalUserCount"]);
                }
                if (DataManager.CheckHasRow(ds.Tables[2]))
                {
                    res.TotalOrderCount = DataManager.GetRowValue_Int(ds.Tables[2].Rows[0]["TotalOrderCount"]);
                }
                if (DataManager.CheckHasRow(ds.Tables[3]))
                {
                    res.TotalOrderAmount = DataManager.GetRowValue_Decimal(ds.Tables[3].Rows[0]["TotalOrderAmount"]);
                }
                if (DataManager.CheckHasRow(ds.Tables[4]))
                {
                    res.WaitSendCount = DataManager.GetRowValue_Int(ds.Tables[4].Rows[0]["WaitSendCount"]);
                }
                if (DataManager.CheckHasRow(ds.Tables[5]))
                {
                    res.WaitServiceCount = DataManager.GetRowValue_Int(ds.Tables[5].Rows[0]["WaitServiceCount"]);
                }

            }
            return res;
        }

        

        public Chart_AddUserModel GetAddUserChart(int shopAdminId)
        {
            var res = new Chart_AddUserModel();
            var tempList = new List<string>();
            var ds = HomeDataAccess.GetAddUserChart(shopAdminId);
            if (DataManager.CheckDs(ds, 1))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        tempList.Add(Convert.ToDateTime(dr["CreateTime"]).ToString("yyyy-MM-dd"));
                    }
                    //开始计算每日的用户增长量
                    foreach (var date in res.DateList)
                    {
                        var _count = tempList.Where(i => i == date).Count();
                        res.DataList.Add(_count);
                    }
                }
            }
            return res;
        }

        

        public Chart_SallModel GetSallChart(int shopAdminId)
        {
            var res = new Chart_SallModel();
            var ds = HomeDataAccess.GetSallChart(shopAdminId);
            if (DataManager.CheckDs(ds, 1))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    var _tempList = new List<Chart_SallItem>();
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        _tempList.Add(new Chart_SallItem() {
                            CreateTime = Convert.ToDateTime(dr["CreateTime"]).ToString("yyyy-MM-dd"),
                            TotalAmount = DataManager.GetRowValue_Decimal(dr["TotalAmount"]),
                            TotalCount = DataManager.GetRowValue_Int(dr["TotalCount"])
                        });
                    }
                    //开始计算图表需要的数据格式
                    res.DateList.ForEach((date) =>
                    {
                        var _item = _tempList.Where(i => i.CreateTime == date).FirstOrDefault();
                        //日期数组（近7日，已经排好序）
                        if (_item != null)
                        {
                            res.OrderAmountList.Add(_item.TotalAmount);
                            res.OrderCountList.Add(_item.TotalCount);
                        }
                        else
                        {
                            //给0
                            res.OrderAmountList.Add(0);
                            res.OrderCountList.Add(0);
                        }
                    });
                }
            }
            return res;
        }

     

    }
}