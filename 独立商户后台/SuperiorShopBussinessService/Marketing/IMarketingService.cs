﻿using SuperiorModel;
using SuperiorModel.Criteria.Marketing;
using System.Collections.Generic;

namespace SuperiorShopBussinessService
{
    public interface IMarketingService:IService
    {

        FullSetModel GetSetModel(int shopAdminId);
        void SetNotPids(string ids, int shopAdminId);
        void SetNotAreas(string ids, int shopAdminId);
        void SetFullAmount(int optionStatus, int amount, int shopAdminId);

        List<CouponModel> GetCouponList(int shopAdminId);
        void AddCoupon(CouponModel model);
        void DelCoupon(int id);
        PagedSqlList<UserCouponModel> SearchUserCoupon(UserCouponCriteria criteria);
       
    }
}
