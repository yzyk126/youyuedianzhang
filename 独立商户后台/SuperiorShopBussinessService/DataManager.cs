﻿using System;
using System.Data;

namespace SuperiorShopBussinessService
{
    public class DataManager
    {
        /// <summary>
        /// 检测dataset是否有指定数量的表
        /// </summary>
        /// <param name="ds"></param>
        /// <param name="tbCount"></param>
        /// <returns></returns>
        public static bool CheckDs(DataSet ds, int tbCount)
        {
            var res = false;
            if (ds != null && ds.Tables.Count == tbCount)
                res = true;
            return res;
        }
        /// <summary>
        /// 检测表里是否有行：当查询单行数据时校验
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static bool CheckHasRow(DataTable dt)
        {
            var res = false;
            if (dt.Rows.Count > 0)
                res = true;
            return res;
        }

        public static decimal GetRowValue_Decimal(object dr)
        {
            return dr == DBNull.Value ? 0.0m : Convert.ToDecimal(dr);
        }
        public static int GetRowValue_Int(object dr)
        {
            return dr == DBNull.Value ? 0 : int.Parse(dr.ToString());
        }
        public static float GetRowValue_Float(object dr)
        {
            return dr == DBNull.Value ? 0f : float.Parse(dr.ToString());
        }
        public static DateTime GetRowValue_DateTime(object dr)
        {
            return dr == DBNull.Value ? Convert.ToDateTime("2010-07-05") : Convert.ToDateTime(dr);
        }
    }
}
