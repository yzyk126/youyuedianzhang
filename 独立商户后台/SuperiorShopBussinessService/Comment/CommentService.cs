﻿using SuperiorModel;
using SuperiorShopDataAccess;
using System;
using System.Collections.Generic;
using System.Data;

namespace SuperiorShopBussinessService
{
    public class CommentService : ICommentService
    {

        public PagedSqlList<CommentModel> Search(SearchCommentCriteria criteria)
        {
            var queryList = new List<CommentModel>();
            var totalCount = 0;
            var ds = CommentDataAccess.Search(criteria);
            if (DataManager.CheckDs(ds, 2))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        queryList.Add(new CommentModel()
                        {
                            Id = Convert.ToInt32(dr["Id"].ToString()),
                            CreateTime = dr["CreateTime"].ToString(),
                            ProductName = dr["ProductName"].ToString(),
                            LoginName = dr["LoginName"].ToString(),
                            ShopName = dr["ShopName"].ToString(),   
                            CommentLevel = Convert.ToInt32(dr["CommentLevel"]),
                            OrderNo = dr["OrderNo"].ToString(),
                            CommentMsg = dr["CommentMsg"].ToString(),
                            HeadImgUrl = dr["HeadImgUrl"].ToString(),
                            NickName = dr["NickName"].ToString(),
                        });
                    }
                }
                totalCount = Convert.ToInt32(ds.Tables[1].Rows[0]["totalCount"]);
            }
            return new PagedSqlList<CommentModel>(queryList, criteria.PagingResult.PageIndex, criteria.PagingResult.PageSize, totalCount);
        }
        public void Del(int id)
        {
            CommentDataAccess.Del(id);
        }

       

        public void Dispose()
        {
            
        }
    }
}
