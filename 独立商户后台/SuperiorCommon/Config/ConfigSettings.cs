﻿using System.Configuration;

namespace SuperiorCommon
{
    public sealed class ConfigSettings
    {
        public static readonly ConfigSettings Instance = new ConfigSettings();

        private ConfigSettings() { }

        public string FileUploadPath
        {
            get
            {
                return ConfigurationManager.AppSettings["FileUploadPath"];
            }
        }

        public string FileUploadFolderNameTemp
        {
            get
            {
                return ConfigurationManager.AppSettings["FileUploadFolderNameTemp"];
            }
        }

        public string WebSetting(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }

        /// <summary>
        /// API的图片网关
        /// </summary>
        public string ApiImgHost
        {
            get
            {
                return ConfigurationManager.AppSettings["ApiImgHost"];
            }
        }
        /// <summary>
        /// 后台图片地址
        /// </summary>
        public string AdminImageHost
        {
            get
            {
                return ConfigurationManager.AppSettings["AdminImageHost"];
            }
        }
        /// <summary>
        /// 图片服务器地址
        /// </summary>
        public string ImageServiceUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["ImageServiceUrl"];
            }
        }


        public string RedisHost
        {
            get
            {
                return ConfigurationManager.AppSettings["RedisHost"];
            }
        }

        public string APPID
        {
            get
            {
                return ConfigurationManager.AppSettings["APPID"];
            }
        }

        public string APP_Secret
        {
            get
            {
                return ConfigurationManager.AppSettings["APP_Secret"];
            }
        }

        public string Merchant_Id
        {
            get
            {
                return ConfigurationManager.AppSettings["Merchant_Id"];
            }
        }
        public string PaySecret
        {
            get
            {
                return ConfigurationManager.AppSettings["PaySecret"];
            }
        }
        

       

        public string AuthRedirectUri
        {
            get
            {
                return ConfigurationManager.AppSettings["AuthRedirectUri"];
            }
        }

    }
}
