﻿namespace SuperiorCommon
{
    public class GroupShopUploader:MinDimensionUploader
    {
        public static readonly GroupShopUploader Instance = new GroupShopUploader();
        private GroupShopUploader() { }

        public override int MinWidth
        {
            get { return 10; }
        }

        public override int MinHeight
        {
            get { return 10; }
        }

        /// <summary>
        /// 50
        /// </summary>
        public override int MaxBytesLength
        {
            get { return 5000 * 1024; }
        }

        public override string[] AllowedImageTypes
        {
            get { return new string[] { ".jpg", ".png", ".jpeg" }; }
        }

        public override string FolderName
        {
            get
            {
                return "GroupShopImage";
                //    ConfigSettings.Instance.FileUploadFolderNameAd;
            }
        }
    }
}
