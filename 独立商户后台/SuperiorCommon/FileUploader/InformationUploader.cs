﻿namespace SuperiorCommon
{
    public class InformationUploader : MinDimensionUploader
    {
        public static readonly InformationUploader Instance = new InformationUploader();
        private InformationUploader() { }

        public override int MinWidth
        {
            get { return 10; }
        }

        public override int MinHeight
        {
            get { return 10; }
        }

        /// <summary>
        /// 50
        /// </summary>
        public override int MaxBytesLength
        {
            get { return 4000 * 1024; }
        }

        public override string[] AllowedImageTypes
        {
            get { return new string[] { ".jpg", ".png", ".jpeg" }; }
        }

        public override string FolderName
        {
            get
            {
                return "InformationImage";
                //    ConfigSettings.Instance.FileUploadFolderNameAd;
            }
        }

    }
}
