﻿namespace SuperiorCommon
{
    public static class StringExtension
    {
        public static string ToForwardSlashPath(this string path)
        {
            return path.Replace('\\', '/');
        }
    }
}
