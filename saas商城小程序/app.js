//app.js
App({
  onLaunch: function () {
    // 展示本地存储能力

  },
  initConfig: function () {
    var that = this;
    return new Promise(function (resolve, reject) {
      if (wx.getExtConfig && that.appSetting.said == "") {
        wx.getExtConfig({
          success(res) {
            const app = getApp();
            that.appSetting.host = res.extConfig.host;
            that.appSetting.spid = res.extConfig.spid;
            that.appSetting.said = res.extConfig.said;
            that.appSetting.name = res.extConfig.name;
            resolve(200);
          },
          fail() {
            resolve(0);
          }
        })
      } else {
        resolve(200);
      }
    });
  },



  globalData: {
    userInfo: null,
    defaultAddress: null,
    isExpress: 1,
    menuid: 0,
    keyword:''
  },
  appSetting: {
    spid: '',
    said: '',
    //host:'http://192.168.0.105:8070'
    host: '',
    name: ''

  }
})