var app= getApp();
var indexRequestClass={};
indexRequestClass.instance = {
    getIndexData:function(){
      wx.showLoading({
        title: '读取中 ',
      })
      var ret;
      wx.request({
        url: app.appSetting.host +"/api/MobileMall/index?ShopID=1AD227F46DFB1666",
        data: {
        },
        header: {
          'content-type': 'application/json' // 默认值
        },
        success: function (res) {
          console.log(res);
          wx.hideLoading();
          ret=res;
        }
      })
      return ret;
    }
};
module.exports = {
  indexRequestClass: indexRequestClass
}