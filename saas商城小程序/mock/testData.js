const app = getApp();

class TestData {
  // static Api = 'https://old.shangshikeji.cn';
  static Api = app.appSetting.host;
  //获取时间戳 http://xx.com/api/MobileAuthority/GetTimeSpan
  //获取三级地址信息

  //验证必须发送给后
  static setParam() {
    // this.GetTimeSpan().then((res)=>{
    //   console.log(res);
    // });
   
    return new Promise((resolve, reject) => {
      if (app.globalData.userInfo) {
        resolve()
      } else {
        wx.getStorage({
          key: 'seltUser',
          success: function (res) {
            if (app.globalData.userInfo == null)
              app.globalData.userInfo = res.data;
            
          },
          fail: function (res) {
            wx.showToast({
              title: '请重新授权',
              icon: 'none'
            })
          }
        })
      }
    })
  }

  static apiInit(json) {
    /*
    json = {
          url: this.Api + "/api/MobileMall/index",
          method: 'GET',
          data: {
            ShopID: app.globalData.ShopID,
          }
    }
    **/
    let self = this;
    return new Promise((resolve, reject) => {
      this.setParam().then(() => {
        let $json = {
          url: json.url,
          method: json.method || 'GET',
          data: Object.assign(json.data || {}, {
            //ShopID: app.globalData.ShopID,
            //CustomerID: app.globalData.CustomerID,
            //Token: app.globalData.userInfo.Token
          }),

        }
        // this.showLoading();
        wx.request({
          url: $json.url,
          method: $json.method,
          data: $json.data,
          success: function (res) {
            if (res.data.Code == 200 && res.data.Message=='') {
              resolve(res.data)
            } else {

              wx.showToast({
                title: res.data.Message,
                icon: 'none',
                duration: 2000
              })
            }
          },
          fail: function () { },
          complete: function () {
            // self.hideLoading();
          }
        })
      })
    });
  }
  static MobileAreaArea(num) {
    this.Api = app.appSetting.host;
    return new Promise((resolve, reject) => {
      this.apiInit({
        url: this.Api + "/api/ConSignee/GetAreaList",
        data: {
          parentId:num==undefined?0:num,
          userid: app.globalData.userInfo.Id
        },
      }).then((res) => {
        resolve(res);
      })
    })
  }
  //保存用户信息
  static MobileConsigneeAdd(json) {
    this.Api = app.appSetting.host;
    Object.assign(json, {
      UserId: app.globalData.userInfo.Id
    })
    return new Promise((resolve, reject) => {
      this.apiInit({
        url: this.Api + "/api/ConSignee/Add",
        method: 'POST',
        data: json,
      }).then((res) => {
        resolve(res);
      })
    })
  }
  //更新地址
  static MobileConsigneeUpdateAddress(json) {
    this.Api = app.appSetting.host;
    Object.assign(json, {
      UserId: app.globalData.userInfo.Id
    })
    return new Promise((resolve, reject) => {
      this.apiInit({
        url: this.Api + "/api/ConSignee/Edit",
        method: 'POST',
        data: json,
      }).then((res) => {
        resolve(res);
      })
    })
  }
  //删除地址
  static MobileConsigneeDelete(id) {
    this.Api = app.appSetting.host;
    return new Promise((resolve, reject) => {
      this.apiInit({
        url: this.Api + "/api/ConSignee/Del",
        data: {
          id: id,
          userid: app.globalData.userInfo.Id
        },
      }).then((res) => {
        resolve(res);
      })
    })
  }
  //更新默认地址
  static MobileConsigneeUpdateState(json) {
    this.Api = app.appSetting.host;
    return new Promise((resolve, reject) => {
      this.apiInit({
        url: this.Api + "/api/ConSignee/UpdateIsDefault",
        data: {
          id: json.Id,
          userid: app.globalData.userInfo.Id,
          isdefault: json.State
        },
      }).then((res) => {
        resolve(res);
      })
    })
  }

  static MobileOrderDetail(id) {
    this.Api = app.appSetting.host;
    return new Promise((resolve, reject) => {
      this.apiInit({
        url: this.Api + "/api/Order/GetDetail",
        data: {
          OrderID: id,
        },
      }).then((res) => {
        resolve(res);
      })
    })
  }

  static MobileOrderGetPayData(orderno) {
    this.Api = app.appSetting.host;
    return new Promise((resolve, reject) => {
      this.apiInit({
        url: this.Api + "/api/Order/GetPayData",
        data: {
          userid: app.globalData.userInfo.Id,
          orderno: orderno
        },
      }).then((res) => {
        resolve(res);
      })
    })
  }

 

}

export default TestData