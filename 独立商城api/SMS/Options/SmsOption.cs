﻿namespace SMS.Options
{
    public class SmsOption
    {
        public string AccessKeyId { get; set; }
        public string Secret { get; set; }
        public string SignName { get; set; }
        public string TemplateCode { get; set; }

    }
}
