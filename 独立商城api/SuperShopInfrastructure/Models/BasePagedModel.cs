﻿namespace SuperShopInfrastructure.Models
{
    public class BasePagedModel
    {
        public int OffSet { get; set; }
        public int Size { get; set; }
    }
}
