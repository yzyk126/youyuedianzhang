﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SuperShopInfrastructure.Models.Product
{
    public class ProductListViewModel
    {
        public string Id { get; set; }
        public string ProductName { get; set; }
        public string Brand { get; set; }
        public string Title { get; set; }
        public string ImgPath { get; set; }
        public decimal SallPrice { get; set; }
        public string ShopAdminId { get; set; }
    }
}
