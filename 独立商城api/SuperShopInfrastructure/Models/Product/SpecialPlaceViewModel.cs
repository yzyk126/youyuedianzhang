﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SuperShopInfrastructure.Models.Product
{
    public class SpecialPlaceViewModel
    {
        public int Id { get; set; }
        public string PlaceName { get; set; }
        public string ImgPath { get; set; }
        public int ShopAdminId { get; set; }
        public int OptionStatus { get; set; }
        public int MenuId { get; set; }

        public string MenuName { get; set; }
        public int Sorft { get; set; }
      
    }
}
