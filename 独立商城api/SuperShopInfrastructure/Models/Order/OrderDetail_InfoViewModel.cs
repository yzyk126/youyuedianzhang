﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SuperShopInfrastructure.Models.Order
{
    public class OrderDetail_InfoViewModel
    {
        public OrderDetail_InfoViewModel()
        {
            this.Products = new List<OrderDetail_ProductItem>();
        }
        public int OrderID { get; set; }
        public string ConsigneeName { get; set; }
        public string ConsigneePhone { get; set; }
        public string Address { get; set; }
        public decimal RealityAmount { get; set; }
        public string OrderNo { get; set; }
        public string Remark { get; set; }
        public int OrderStatus { get; set; }
        public string CreateTime { get; set; }
        public string PayTime { get; set; }
        public decimal CouponDelAmount { get; set; }
        public decimal ShippingAmount { get; set; }
        public decimal OrderAmount { get; set; }
        public string CouponName { get; set; }
        /// <summary>
        /// 快递名称
        /// </summary>
        public string DeliveryName { get; set; }
        /// <summary>
        /// 快递单号
        /// </summary>
        public string DeliveryNo { get; set; }
        public List<OrderDetail_ProductItem> Products { get; set; }
        /// <summary>
        /// 订单评论ID
        /// </summary>
        public int CommentId { get; set; }
    }

    public class OrderDetail_ProductItem
    {
        public string ProductId { get; set; }
        public string ProductName { get; set; }
        public string ProppetyCombineName { get; set; }
        public int BuyNum { get; set; }
        public decimal SalePrice { get; set; }
        public string ImagePath { get; set; }
    }
}
