﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuperShopInfrastructure.Models.Ship
{
    public class FullSetViewModel
    {
        public int Id { get; set; }

        public int Amount { get; set; }
        public int OptionStatus { get; set; }
        public string NotHasProductIds { get; set; }
        public string NotHasProvinceIds { get; set; }
        public List<string> NotHasProductImgs { get; set; }
        public List<string> NotHasAreaNames { get; set; }

        public List<string> NotHasProductIdList
        {
            get
            {
                if (string.IsNullOrEmpty(this.NotHasProductIds))
                {
                    return new List<string>();
                }
                else
                {
                    return this.NotHasProductIds.Split(',').ToList();
                }


            }
        }

        public List<string> NotHasProvinceIdList
        {
            get
            {
                if (string.IsNullOrEmpty(this.NotHasProvinceIds))
                {
                    return new List<string>();
                }
                else
                {
                    return this.NotHasProvinceIds.Split(',').ToList();
                }


            }
        }
    }
}
