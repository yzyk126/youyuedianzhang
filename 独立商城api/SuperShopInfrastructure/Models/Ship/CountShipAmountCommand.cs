﻿using SuperShopInfrastructure.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace SuperShopInfrastructure.Models.Ship
{
    public class CountShipAmountCommand
    {
        public List<CountShipItem> Products { get; set; }
        [Required]
        [Decrypt]
        public string Spid { get; set; }
        [Required]
        [Decrypt]
        public string UserId { get; set; }
        public decimal TotalProductAmount { get; set; }
        public string ProvinceCode { get; set; }
    }

    public class CountShipItem
    {
        public string ProvinceCode { get; set; }
        public string ProductName { get; set; }
        public string ProductId { get; set; }
        public int SaleNum { get; set; }
        public int Product_SkuId { get; set; }
        public float Weight { get; set; }
        public int ShippingTemplateId { get; set; }
        public ShippingTemplateModel ShippingTemplateModel { get; set; }
        /// <summary>
        /// 该商品的运费模板下，送达的所有省份
        /// </summary>
        public List<string> ShipAllProvinceIds
        {
            get
            {
                var res = new List<string>();
                if (this.ShippingTemplateModel == null)
                    return res;
                this.ShippingTemplateModel.Items.ForEach((item) =>
                {
                    res.AddRange(item.ProvinceIdList);
                });
                return res;
            }
        }
        /// <summary>
        /// 该商品，所属当前收货人省份的子模板
        /// </summary>
        public ShippingTemplateItem CurrentShipItem
        {
            get
            {
                if (this.ShippingTemplateModel == null)
                    return null;
                return this.ShippingTemplateModel.Items.Where(i => i.ProvinceIdList.Contains(this.ProvinceCode)).FirstOrDefault();
            }
        }
    }

    public class ShippingTemplateModel
    {
        public ShippingTemplateModel()
        {
            this.Items = new List<ShippingTemplateItem>();
        }
        public int Id { get; set; }
        public string ShippingTemplatesName { get; set; }
        public int ChargType { get; set; }
        public int ShopAdminId { get; set; }

        public DateTime CreateTime { get; set; }

        public List<ShippingTemplateItem> Items { get; set; }
    }

    public class ShippingTemplateItem
    {
        public int Id { get; set; }
        public int ShippingTemplateId { get; set; }
        public string ProvinceIds { get; set; }
        public string ProvinceStr { get; set; }
        public int FirstValue { get; set; }
        public decimal FirstAmount { get; set; }
        public int NextValue { get; set; }
        public decimal NextAmount { get; set; }

        public List<string> ProvinceIdList
        {
            get
            {
                if (string.IsNullOrEmpty(this.ProvinceIds))
                {
                    return new List<string>();
                }
                else
                {
                    return this.ProvinceIds.Split(',').ToList();
                }

            }
        }

    }
}
