﻿using SuperShopInfrastructure.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SuperShopInfrastructure.Models.Question
{
    public class CreateQuestionCommand//deleted
    {
        [Required]
        [Decrypt]
        public string ShopAppId { get; set; }
        [Required]
        [Decrypt]
        public string ShopAdminId { get; set; }
        [Required]
        [Decrypt]
        public string UserId { get; set; }
        public string Question { get; set; }
    }
}
