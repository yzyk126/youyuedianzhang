﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SuperShopInfrastructure.Models.Coupon
{
    public class CouponViewModel
    {
        public int Id { get; set; }
        public string CouponName { get; set; }
        public int CouponType { get; set; }
        public decimal DelAmount { get; set; }
        public int Discount { get; set; }
        public decimal MinOrderAmount { get; set; }
        public int Days { get; set; }
        public int TotalCount { get; set; }
    }
}
