﻿using CqCore.DB;
using SuperShopInfrastructure.Extensions;
using SuperShopInfrastructure.Models.Question;
using SuperShopRepository.Dbs;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace SuperShopRepository
{
    public class QuestionRepository
    {
        private readonly ISqlService<SuperShopCustomerDb> sqlService;

        public QuestionRepository(ISqlService<SuperShopCustomerDb> sqlService)
        {
            this.sqlService = sqlService;
        }

        public async Task Insert(CreateQuestionCommand model)
        {
            SqlParameter[] parms =
          {
                 new SqlParameter("@ShopAppId", model.ShopAppId.ToInt()),
                     new SqlParameter("@ShopAdminId", model.ShopAdminId.ToInt()),
                         new SqlParameter("@UserId", model.UserId.ToInt()),
                             new SqlParameter("@Question", model.Question)
            };
            var sql = "insert into C_Question(ShopAppId,ShopAdminId,UserId,Question)values(@ShopAppId,@ShopAdminId,@UserId,@Question)";
            await sqlService.ExecuteNonQuery(System.Data.CommandType.Text, sql, parms);
        }
    }
}
