﻿using System.Threading.Tasks;

namespace CqCore.Messaging
{
    public interface IMessageHandler<TMessage>
    {
        Task HandleAsync(TMessage message);
    }
}
