﻿using System;
using System.IO;

namespace CqBuiltinService.Extensions
{
    public static class AppDomainExtensions
    {
        public static string GetFilePath(this AppDomain context, string relative)
        {
            return Path.Combine(context.BaseDirectory, relative);
        }
    }
}
