﻿using CqBuiltinService.Options;
using CqCore.Extensions;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.Swagger;
using System;

namespace CqBuiltinService.Documents
{
    public static class DocumentConfiguratorExtensions
    {
        public static IServiceCollection AddBuiltinDocument(this IServiceCollection services)
        {
            var provider = services.BuildServiceProvider();
            var documentConfig = provider.GetRequiredService<IOptions<DocumentOption>>().Value;

            services.AddSwaggerGen(options =>
            {
                options.IncludeXmlComments($"{AppDomain.CurrentDomain.BaseDirectory}{AppDomain.CurrentDomain.GetApplicationName()}.xml");

                options.SwaggerDoc(documentConfig.Version, new Info
                {
                    Title = $"{AppDomain.CurrentDomain.GetApplicationName()}",
                    Version = $"{documentConfig.Version}"
                });
            });

            return services;
        }
    }
}
