﻿using CqBuiltinService.Options;
using CqCore.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using System;

namespace CqBuiltinService.Documents
{
    public static class DocumentBuilderExtensions
    {
        public static IApplicationBuilder UseBuiltinDocument(this IApplicationBuilder app)
        {
            var documentConfig = app.ApplicationServices.GetRequiredService<IOptions<DocumentOption>>().Value;

            app.UseSwagger(options =>
            {
                options.RouteTemplate = "well-known/docs/{documentName}/swagger.json";
            });

            app.UseReDoc(options =>
            {
                options.RoutePrefix = "well-known/docs";
                options.SpecUrl = $"{documentConfig.Version}/swagger.json";
                options.DocumentTitle = $"{AppDomain.CurrentDomain.GetApplicationName()} API Docs";
            });

            return app;
        }
    }
}
