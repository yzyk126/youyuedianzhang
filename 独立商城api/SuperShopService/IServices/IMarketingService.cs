﻿using CqCore.Commands;
using SuperShopInfrastructure.Models.Coupon;
using SuperShopInfrastructure.Models.Ship;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SuperShopService.IServices
{
    public interface IMarketingService
    {
        Task<List<UserCouponViewModel>> GetUserCouponList(int userid);
        Task<UserCouponViewModel> GetUserCoupon(int userid, int couponid);
        Task<List<CouponViewModel>> GetCouponList(int shopAdminId);
        Task<CommandResult<bool>> GetCoupon(int userid, int couponid);
        Task<FullSetViewModel> GetSetModel(int shopAdminId);
    }
}
