﻿using SuperShopInfrastructure.Models.Question;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SuperShopService.IServices
{
    public interface IQuestionService
    {
        Task Insert(CreateQuestionCommand model);
    }
}
