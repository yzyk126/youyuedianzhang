﻿using CqCore.Commands;
using SuperShopInfrastructure.Models.Comment;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SuperShopService.IServices
{
    public interface ICommentService
    {
        Task<CommandResult<bool>> Create(CreateCommentCommand command);
        Task<List<CommentViewModel>> QueryComments(QueryCommentCriteria criteria);
        Task<int> GetCounts(int productId);
    }
}
