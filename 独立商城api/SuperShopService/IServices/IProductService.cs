﻿using CqCore.Commands;
using SuperShopInfrastructure.Models.Order;
using SuperShopInfrastructure.Models.Product;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SuperShopService.IServices
{
    public interface IProductService
    {
        Task<List<MenuItemViewModel>> GetMenuList(int shopAdminId);
        Task<List<ProductListViewModel>> QueryProductList(QueryProductListCriteria criteria);
        Task<ProductDetailViewModel> GetProduct(int id);
        Task<List<CreaterOrder_CheckItem>> GetCreateOrder_CheckItemList(string skuids);
        Task<ProductSkuManagerModel> GetProductSkuManagerModel(int shopAdminId, string productId);
        Task<ProductSkuManagerModel> GetProductSkuManagerModel_AJAX(int shopAdminId, string productId);
        Task<List<SpecialPlaceViewModel>> SpecialPlaceList(int shopAdminId,int type);
        Task<CommandResult<string>> MakeOroductQrImage(MakeProductQrImageCommand command);
        Task<HomeProductDataViewModel> GetHomeData(int shopAppId,int said);
    }
}
