﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SuperShopInfrastructure.Models.ShopCart;
using SuperShopInfrastructure.Static;
using SuperShopService.IServices;

namespace SuperShopApi.Controllers
{
    /// <summary>
    ///  /// <summary>
    /// 整个购物车不与数据库或者redis交互 ，以免数据量过大频繁的增删改锁表，每次加载购物车时，只根据用户保存在本地的购物车skuid，来查询最新的数据模型给客户端展示。
    /// </summary>
    /// </summary>
    [Route("[controller]")]
    [ApiController]
    public class ShopCartController : ControllerBase
    {
        private readonly IShopCartService shopCartService;

        public ShopCartController(IShopCartService shopCartService)
        {
            this.shopCartService = shopCartService;
        }

        /// <summary>
        ///  通过用户本地购物车存储的skuids比如1,2,3来换取最新的购物车所需商品模型的集合
        /// </summary>
        /// <param name="userid"></param>
        /// <param name="token"></param>
        /// <param name="skuids"></param>
        /// <returns></returns>
        [HttpGet("")]
        [ProducesResponseType(typeof(List<ShopCartListViewModel>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetShopCartModelList([FromQuery] string userid, [FromQuery] string token, [FromQuery] string skuids)
        {
            string _userid = ToolManager.DecryptParms(userid);
            if (_userid == null)
                return BadRequest("非法用户参数");
            var res = await shopCartService.GetShopCartList(skuids);
            if (res == null)
                return BadRequest("非法参数");
            return Ok(res);
        }

    }
}