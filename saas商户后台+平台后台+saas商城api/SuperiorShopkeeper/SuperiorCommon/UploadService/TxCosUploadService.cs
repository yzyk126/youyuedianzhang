﻿using COSXML;
using COSXML.Auth;
using COSXML.Model.Object;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperiorCommon
{
    public class TxCosUploadService
    {
        private static string AppId = "1305248370";
        private static string Region = "ap-beijing";
        private static string SecretId = "AKIDTlgroP8PYAZjhSCJU2lKbcTAbvzonwuK";
        private static string SecretKey = "QfGaqpZ1DWhYA8sm36ug9aZGidKB1hRz";
        private static string Bucket = "course-1305248370";
        private static string ImgDic = "CourseImg";
        private static string VideoDic = "CourseVideo";
        private static string Host = "https://course-1305248370.cos.ap-beijing.myqcloud.com";

        private static CosXml cosXml;

        public static CosXml CosXml
        {
            get
            {
                if (CosXml == null)
                {
                    CosXmlConfig config = new CosXmlConfig.Builder()
                     .IsHttps(true)  //设置默认 HTTPS 请求
                     .SetRegion(Region)  //设置一个默认的存储桶地域
                     .SetDebugLog(true)  //显示日志
                     .Build();  //创建 CosXmlConfig 对象
                        long durationSecond = 600;  //每次请求签名有效时长，单位为秒
                        QCloudCredentialProvider cosCredentialProvider = new DefaultQCloudCredentialProvider(
                        SecretId, SecretKey, durationSecond);

                    cosXml = new CosXmlServer(config, cosCredentialProvider);
                }
                return cosXml;
            }
        }

        public static List<string> Upload(List<string> imgs, IFileUploader uploader = null)
        {
            var res = new List<string>();
            foreach (var item in imgs)
            {
                res.Add(Upload(item));
            }
            return res;
        }

        public static string Upload(string img, IFileUploader uploader = null)
        {
            try
            {
                var _img = Path.GetFileName(img);
                var tempPhysicalPath = Path.Combine(ConfigSettings.Instance.FileUploadPath, ConfigSettings.Instance.FileUploadFolderNameTemp, _img);
                byte[] data = System.IO.File.ReadAllBytes(tempPhysicalPath);

                var _cosPath = $"{ImgDic}/{DateTime.Now.ToString("yyyyMMdd")}/{_img}";
                PutObjectRequest putObjectRequest = new PutObjectRequest(Bucket, _cosPath, data);
                CosXml.PutObject(putObjectRequest);
                return $"{Host}/{_cosPath}";
            }
            catch (COSXML.CosException.CosClientException clientEx)
            {
                //请求失败
                throw new Exception("CosClientException: " + clientEx);
            }
            catch (COSXML.CosException.CosServerException serverEx)
            {
                throw new Exception("CosServerException: " + serverEx.GetInfo());
                //请求失败
            }

        }
    }
}
