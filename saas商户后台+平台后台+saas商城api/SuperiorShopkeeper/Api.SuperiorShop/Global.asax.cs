﻿using System.Web.Http;

namespace Api.SuperiorShop
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            AutofacConfig.ConfigInitance();
        }
    }
}
