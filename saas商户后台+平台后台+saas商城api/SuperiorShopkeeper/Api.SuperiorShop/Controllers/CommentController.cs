﻿using System.Web.Http;
using SuperiorModel;
using SuperiorShopBussinessService;
using Redis;

namespace Api.SuperiorShop.Controllers
{
    [ApiExceptionAttribute]
    public class CommentController : ApiControllerBase
    {
        private readonly ICommentService _ICommentService;

        public CommentController(ICommentService ICommentService)
        {
            _ICommentService = ICommentService;

        }
        protected override void Dispose(bool disposing)
        {
            this._ICommentService.Dispose();
            base.Dispose(disposing);
        }
        // GET: Comment
        /// <summary>
        /// 添加评论
        /// </summary>
        /// <param name="parms"></param>
        /// <returns></returns>
        [HttpPost]
        public ApiResultModel<object> Add(Api_AddCommentParms criteria)
        {
            if (string.IsNullOrEmpty(criteria.Spid) || string.IsNullOrEmpty(criteria.UserId) || string.IsNullOrEmpty(criteria.Said) || string.IsNullOrEmpty(criteria.CommentMsg))
                return Error("参数错误");
            var _spid = base.ValidataParms(criteria.Spid);
            if (_spid == null)
                return Error("非法参数");
            criteria.Spid = _spid;
            var _userid = base.ValidataParms(criteria.UserId);
            if (_userid == null)
                return Error("非法参数");
            criteria.UserId = _userid;
            var _said = base.ValidataParms(criteria.Said);
            if (_said == null)
                return Error("非法参数");
            criteria.Said = _said;
            string msg = "";
            var ret = _ICommentService.Add(criteria,out msg);
            if (ret != 1)
                return Error(msg);
            //删除订单详情redis
            RedisManager.Remove("GetDetail_"+criteria.OrderId);
            return Success(true);
        }
        /// <summary>
        /// 获取评论分页列表
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        [HttpPost]
        public ApiResultModel<object> GetPagedList(Api_CommentCriteria criteria)
        {
            if (string.IsNullOrEmpty(criteria.ProductId))
                return Error("非法参数");
       
            var _productId = base.ValidataParms(criteria.ProductId);
            if (_productId == null)
                return Error("非法参数");
            criteria.ProductId = _productId;
            var res = _ICommentService.Api_GetCommentPagedList(criteria);
            return Success(res);
        }
        /// <summary>
        /// 获取该商品评价总数
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        public ApiResultModel<object> GetCount(string productId)
        {
            var _productId = base.ValidataParms(productId);
            if (_productId == null)
                return Error("非法参数");
            var res = _ICommentService.GetCounts(_productId.ToInt());
            return Success(res);
        }     
    }
}