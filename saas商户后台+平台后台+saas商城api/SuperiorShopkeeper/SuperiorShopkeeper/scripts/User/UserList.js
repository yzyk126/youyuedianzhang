﻿(function () {
    UserListClass = {};
    var _layedit;
    var _searchCriteria = { PagingResult: { PageIndex: 0, PageSize: 30 }, AppType: 999, OptionStatus:999 };
    UserListClass.Instance = {
        Init: function () {
            layui.use('form', function () {
                var form = layui.form;
                form.render();
            });
            layui.use('laydate', function () {
                var laydate = layui.laydate;
                laydate.render({
                    elem: '#search_BeginTime', //指定元素
                    type: 'datetime'
                    , theme: 'grid'
                });
                laydate.render({
                    elem: '#search_EndTime', //指定元素
                    type: 'datetime'
                    , theme: 'grid'
                });
            });
            //分页事件注册
            superior.ui.control.Pager.enablePaging(document, UserListClass.Instance.Refresh);
            $(document).on('click', '#btn_search', this.Search);
            $(document).on('click', '.OptionProduct', this.OptionStatus);
        },
        OptionStatus: function () {
            var id = $(this).attr("OptionId");
            var opt = $(this).attr("Opt");
            var index = layer.load(1);
            $.get("/User/OptionUser?id=" + id + "&optionStatus=" + opt + "&r=" + new Date().getTime(), function (data) {
                layer.close(index);
                if (data.IsSuccess) {
                    layer.alert("执行成功", function () {
                        window.location.href = "/User/List";
                    })

                } else {
                    layer.alert(data.Message);
                }
            })
        },
        Search: function () {

            _searchCriteria.NickName = $("#search_NickName").val();
            _searchCriteria.AppType = $("#search_AppType").val();
            _searchCriteria.ShopAppId = $.trim($("#search_ShopAppId").val());
            _searchCriteria.OptionStatus = $("#search_status").val();
            _searchCriteria.BeginTime = $("#search_BeginTime").val();
            _searchCriteria.EndTime = $("#search_EndTime").val();
            UserListClass.Instance.Refresh(0);
        },
        Refresh: function (pageIndex) {
            var url = "/User/List";
            if (pageIndex !== undefined)
                _searchCriteria.PagingResult.PageIndex = pageIndex;
            var index = layer.load(1);
            RequestManager.Ajax.Post("/User/List", _searchCriteria, true, function (data) {
                layer.close(index);
                $("#dataList").html(data);
            })
        }
    };
})()