﻿(function () {
    RechargeClass = {};
    var _id = 0;
    RechargeClass.Instance = {
        Init: function () {
            $(document).on('click', '.buy_side', this.Chose);
            
            $(document).on('click', '#btnBuy', this.Submit);
        },
        Submit: function () {
            _id = $(".chose").attr("rid");
            layer.open({
                type: 2,
                title: '微信扫一扫付款',
                shadeClose: true,
                shade: 0.8,
                area: ['380px', '450px'],
                content: '/Home/CommitRecharge?id='+_id //iframe的url
            });
        },
        Chose: function () {
            $(".buy_side").each(function () {
                $(this).removeClass('chose');
            })
            $(this).addClass("chose");
            var title = $(this).attr("rtitle");
            var amount = $(this).attr("ramount");
            var apptype = $(this).attr("appType");
            var _temp = "";
            switch (apptype)
            {
                case "1":
                    _temp = "小程序商城";
                    break;
                case "2":
                    _temp = "H5商城";
                    break;
            }
            $("#msgSpan").val(_temp + "(" + title + ")");
            $("#amountSpan").val("￥" + amount + "");
            _id = $(this).attr("rid");
        }

    };
})()