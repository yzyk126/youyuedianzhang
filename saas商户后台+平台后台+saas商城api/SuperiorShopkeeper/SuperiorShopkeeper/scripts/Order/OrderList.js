﻿(function () {
    OrderListClass = {};
    var _layedit;
    var _searchCriteria = { PagingResult: { PageIndex: 0, PageSize: 30 }, AppType: 999, OrderStatus: 999, ShopAppId: "unChecke" };
    var _orderid = "";
    OrderListClass.Instance = {
        Init: function () {
            layui.use('form', function () {
                var form = layui.form;
                form.render();
                form.on('submit(btn_submit)', OrderListClass.Instance.SubmitSend);//确认发货
            });
            layui.use('laydate', function () {
                var laydate = layui.laydate;
                laydate.render({
                    elem: '#search_BeginTime', //指定元素
                    type: 'datetime'
                    , theme: 'grid'
                });
                laydate.render({
                    elem: '#search_EndTime', //指定元素
                    type: 'datetime'
                    , theme: 'grid'
                });
            });
            //分页事件注册
            superior.ui.control.Pager.enablePaging(document, OrderListClass.Instance.Refresh);
            $(document).on('click', '#btn_search', this.Search);
            $(document).on('click', '.send_order', this.Send);
            $(document).on('click', '.doservice', this.DoService);
            $(document).on('click','')
        },
        SubmitSend: function () {
            var deliveryName = $("#text_deliveryName").val();
            var deliveryNo = $("#text_deliveryNo").val();
            var model = {};
            model.Id = _orderid,
            model.DeliveryName = deliveryName;
            model.DeliveryNo = deliveryNo;
            layer.confirm('确定已经发货了么？', {
                btn: ['确定', '取消']
            }, function () {
                var index = layer.load(1);
                RequestManager.Ajax.Post("/Order/SendProduct", model, true, function (data) {
                    layer.close(index);
                    if (data.IsSuccess) {
                        layer.alert("发货成功", function () {
                            OrderListClass.Instance.Search();
                            layer.closeAll();
                        })
                    } else {
                        layer.alert(data.Message);
                    }
                })
              
            }, function () {

            });
        },
        Send: function () {
            _orderid = $(this).attr("OptionId");
            layer.open({
                type: 1,
                skin: 'layui-layer-rim', //加上边框
                area: ['420px', '300px'], //宽高
                content: OrderListClass.Instance.CreateHtml()
            });
        },

        CreateHtml: function () {
            var arr = [];
            arr.push('<div class="layui-form" action="" >');
            arr.push('<div class="layui-form-item">');
            arr.push('<label for="text_deliveryName" class="layui-form-label">快递名称</label>');
            arr.push('<div class="layui-input-inline" style="width:50%">');
            arr.push('<input type="text" id="text_deliveryName" name="text_deliveryName" lay-verify="required" class="layui-input" placeholder="请输快递名称">');
            arr.push('</div>');
            arr.push(' <div class="layui-form-mid layui-word-aux"><span class="x-red">*</span></div>');
            arr.push('</div>');

            arr.push('<div class="layui-form-item">');
            arr.push(' <label for="text_deliveryNo" class="layui-form-label">快递单号</label>');
            arr.push('<div class="layui-input-inline" style="width:50%">');
            arr.push(' <input type="text" id="text_deliveryNo" name="text_deliveryNo" lay-verify="required" value="" class="layui-input" placeholder="请输快递单号">');
            arr.push('</div>');
            arr.push(' <div class="layui-form-mid layui-word-aux"><span class="x-red">*</span></div>');
            arr.push('</div>');
            arr.push('<div class="layui-form-item" style="text-align:center;">');
            arr.push('<div class="layui-input-block" style="margin:0;"><button class="layui-btn" lay-submit lay-filter="btn_submit">确定</button></div>');
            arr.push('</div>');
            arr.push('</div>');
            return arr.join('');
        },

        DoService:function(){
            var id = $(this).attr("OptionId");
            layer.confirm('确定已处理完售后么？', {
                btn: ['确定', '取消']
            }, function () {
                var index = layer.load(1);
                $.get("/Order/FinishService?orderid=" + id + "&r=" + new Date().getTime(), function (data) {
                    layer.close(index);
                    if (data.IsSuccess) {
                        layer.alert("处理成功", function () {
                            OrderListClass.Instance.Search();
                            layer.closeAll();
                        })

                    } else {
                        layer.alert(data.Message);
                    }
                })
            }, function () {

            });
        },
        Search: function () {

            _searchCriteria.OrderNo = $("#search_OrderNo").val();
            _searchCriteria.AppType = $("#search_AppType").val();
            _searchCriteria.ShopAppId = $.trim($("#search_ShopAppId").val());
            _searchCriteria.OrderStatus = $("#search_status").val();
            _searchCriteria.BeginTime = $("#search_BeginTime").val();
            _searchCriteria.EndTime = $("#search_EndTime").val();
            OrderListClass.Instance.Refresh(0);
        },
        Refresh: function (pageIndex) {
            var url = "/Order/List";
            if (pageIndex !== undefined)
                _searchCriteria.PagingResult.PageIndex = pageIndex;
            var index = layer.load(1);
            RequestManager.Ajax.Post("/Order/List", _searchCriteria, true, function (data) {
                layer.close(index);
                $("#dataList").html(data);
            })
        }
    };
})()