﻿(function () {
    CheckVersionClass = {};
    CheckVersionClass.Instance = {
        Init: function () {
            $(function () {
                var msg = $("#hidden_msg").val();
                if (msg != "") {
                    layer.alert(msg);
                }
            })
            $(document).on('click', '#btn_getQrCode', this.GetQrCode);
            $(document).on('click', '#btn_submit', this.SubmitCode);
            $(document).on('click', '#btn_getResult', this.GetCheckResult);
            $(document).on('click', '#btn_reset', this.Reset);
            $(document).on('click', '#btn_release',this.Release)
        },
        Release:function(){
            var index = layer.load(1);
            $.get("/ShopApp/Release?id=" + ToolManager.Common.UrlParms("id") + "&r=" + new Date().getTime(), function (data) {
                layer.close(index);
                if (data.IsSuccess) {
                    layer.alert("发布成功!", function () {
                        window.location.href = "/ShopApp/CheckVersion?id=" + ToolManager.Common.UrlParms("id");
                    });

                } else {
                    layer.alert(data.Message);
                }
            })
        },
        Reset:function(){
            layer.confirm('重置后将要重新上传代码？', {
                btn: ['确定', '取消'] //按钮
            }, function () {
                var index = layer.load(1);
                $.get("/ShopApp/ResetProcess?id=" + ToolManager.Common.UrlParms("id") + "&r=" + new Date().getTime(), function (data) {
                    layer.close(index);
                    if (data.IsSuccess) {
                        layer.alert("重置成功!", function () {
                            window.location.href = "/ShopApp/CheckVersion?id=" + ToolManager.Common.UrlParms("id");
                        });

                    } else {
                        layer.alert(data.Message);
                    }
                })
            }, function () {
                
            });
        },
        GetCheckResult: function () {
            var index = layer.load(1);
            $.get("/ShopApp/GetCheckResult?id=" + ToolManager.Common.UrlParms("id") + "&r=" + new Date().getTime(), function (data) {
                layer.close(index);
                if (data.IsSuccess) {
                    switch (data.Data.status) {
                        case 0:
                            layer.alert("审核成功,请点击发布小程序,发布上线!", function () {
                                window.location.href = "/ShopApp/CheckVersion?id=" + ToolManager.Common.UrlParms("id");
                            });
                            break;
                        case 1:
                            layer.alert("审核失败,原因:" + data.Data.reason + "", function () {
                                window.location.href = "/ShopApp/CheckVersion?id=" + ToolManager.Common.UrlParms("id");
                            });
                            break;
                        case 2:
                            layer.alert("审核中,请稍后查询");
                            break;
                    }
                  

                } else {
                    layer.alert(data.Message);
                }
            })
        },
        SubmitCode: function () {
            layer.prompt({ title: '请输入小程序标签', formType: 0 }, function (data, index) {
                if (data.length == "" || data.length > 20) {
                    layer.alert("标签必填,多个标签用空格分隔，标签不能多于10个，标签长度不超过20");
                    return false;
                }
                var index = layer.load(1);
                $.get("/ShopApp/SubmitCode?id=" + ToolManager.Common.UrlParms("id") + "&tag=" + data + "&r=" + new Date().getTime(), function (data) {
                    layer.close(index);
                    if (data.IsSuccess) {
                        layer.alert("提交审核成功,大概2小时后请获取审核结果,请勿重复上传代码并提交!", function () {
                            window.location.href = "/ShopApp/CheckVersion?id=" + ToolManager.Common.UrlParms("id");
                        });

                    } else {
                        layer.alert(data.Message);
                    }
                })

            });

        },
        GetQrCode: function () {
            var index = layer.load(1);
            $.get("/ShopApp/GetQrCode?id=" + ToolManager.Common.UrlParms("id") + "&r=" + new Date().getTime(), function (data) {
                layer.close(index);
                if (data.IsSuccess) {
                    layer.open({
                        type: 1,
                        skin: 'layui-layer-rim', //加上边框
                        area: ['350px', '350px'], //宽高
                        content: '<div><img style="width:330px;height:310px;"  src="' + data.Data + '"></div>'
                    });

                } else {
                    layer.alert(data.Message);
                }
            })
        }
    };
})();