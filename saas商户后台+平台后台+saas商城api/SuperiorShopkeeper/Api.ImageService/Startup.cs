﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Api.ImageService.Startup))]

namespace Api.ImageService
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
