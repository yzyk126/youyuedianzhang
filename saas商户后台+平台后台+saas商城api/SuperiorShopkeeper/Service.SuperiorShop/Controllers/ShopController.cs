﻿using SuperiorCommon;
using SuperiorModel;
using SuperiorShopBussinessService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Service.SuperiorShop.Controllers
{
    [JsonException]
    public class ShopController : BaseController
    {
        private readonly IShopAdminService _IShopAdminService;

        public ShopController(IShopAdminService IShopAdminService)
        {
            _IShopAdminService = IShopAdminService;
        }
        protected override void Dispose(bool disposin)
        {
            this._IShopAdminService.Dispose();
            base.Dispose(disposin);
        }
        // GET: Shop
        [CheckShopLoginFitler]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Login()
        {
            return View();
        }

        /// <summary>
        /// 登录验证码
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        public FileContentResult CaptchaImage()
        {
            var captcha = new LiteralCaptcha(115, 48, 4);
            var bytes = captcha.Generate();
            Session["captcha"] = captcha.Captcha;
            return new FileContentResult(bytes, "image/jpeg");
        }

        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [AjaxOnly]
        [HttpPost]
        public JsonResult Login(ShopLoginParams model)
        {
            if (!ModelState.IsValid)
                return Error(base.ErrorMsg());
            var captcha = Session["captcha"];
            if (captcha == null)
                return Error("验证码错误");
            string strcaptcha = Convert.ToString(captcha);
            if (strcaptcha != model.Code)
                return Error("验证码错误");
            //查询店铺数据
            var shop = _IShopAdminService.GetAdminModel(model.UserName, MD5Manager.MD5Encrypt(model.PassWord));
            if (string.IsNullOrEmpty(shop.Id))
                return Error("账号或密码错误");
            shop.ShopAdminType = 1;
            SessionManager.SetSession(SessionKey.ServiceShopKey, shop);
            return Success(true);
        }

        
    }
}