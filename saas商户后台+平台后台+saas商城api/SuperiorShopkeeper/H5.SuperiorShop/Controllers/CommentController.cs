﻿using System.Web.Mvc;
using SuperiorModel;
using SuperiorShopBussinessService;
using Redis;

namespace H5.SuperiorShop.Controllers
{
    [CheckLoginFitler]
    [JsonException]
    public class CommentController : BaseController
    {
        private readonly ICommentService _ICommentService;

        public CommentController(ICommentService ICommentService)
        {
            _ICommentService = ICommentService;

        }
        protected override void Dispose(bool disposing)
        {
            this._ICommentService.Dispose();
            base.Dispose(disposing);
        }
        // GET: Comment
        /// <summary>
        /// 添加评论
        /// </summary>
        /// <param name="parms"></param>
        /// <returns></returns>
        /// 
        [AjaxOnly]
        [HttpPost]
        public JsonResult Add(Api_AddCommentParms criteria)
        {
            if (string.IsNullOrEmpty(criteria.Spid) || string.IsNullOrEmpty(criteria.Said) || string.IsNullOrEmpty(criteria.CommentMsg))
                return Error("参数错误");
            var _spid = base.ValidataParms(criteria.Spid);
            if (_spid == null)
                return Error("非法参数");
            criteria.Spid = _spid;
            criteria.UserId = UserContext.DeUserId;
            var _said = base.ValidataParms(criteria.Said);
            if (_said == null)
                return Error("非法参数");
            criteria.Said = _said;
            string msg = "";
            var ret = _ICommentService.Add(criteria, out msg);
            if (ret != 1)
                return Error(msg);
            //删除订单详情redis
            RedisManager.Remove("GetDetail_" + criteria.OrderId);
            return Success(true);
        }
        /// <summary>
        /// 获取评论分页列表
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        /// 
        [AjaxOnly]
        [HttpPost]
        public JsonResult GetPagedList(Api_CommentCriteria criteria)
        {
            if (string.IsNullOrEmpty(criteria.ProductId))
                return Error("非法参数");
            var _productId = base.ValidataParms(criteria.ProductId);
            if (_productId == null)
                return Error("非法参数");
            criteria.ProductId = _productId;
            var res = _ICommentService.Api_GetCommentPagedList(criteria);
            return Success(res);
        }
        /// <summary>
        /// 获取该商品评价总数
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        /// 
        [AjaxOnly]
        public JsonResult GetCount(string productId)
        {
            var _productId = base.ValidataParms(productId);
            if (_productId == null)
                return Error("非法参数");
            var res = _ICommentService.GetCounts(_productId.ToInt());
            return Success(res);
        }
        /// <summary>
        /// 评价列表
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult List(string id)
        {
            var  _id = base.ValidataParms(id);
            var criteria = new Api_CommentCriteria() {
                OffSet=0,
                ProductId = _id,
                Size = 8
            };
            var model = _ICommentService.Api_GetCommentPagedList(criteria);
            return View(model);
        }
        /// <summary>
        /// 搜索评价
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        /// 
        [HttpPost]
        [AjaxOnly]
        public JsonResult SearchList(Api_CommentCriteria criteria)
        {
            criteria.ProductId = base.ValidataParms(criteria.ProductId);
            var model = _ICommentService.Api_GetCommentPagedList(criteria);
            return Success(model);
        }

        /// <summary>
        /// 添加评论页面
        /// </summary>
        /// <returns></returns>
        public ActionResult Add()
        {
            return View();
        }
    }
}