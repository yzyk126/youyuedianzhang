﻿using System;
using System.Linq;
using System.Web.Mvc;
using Redis;
using SuperiorModel;
using SuperiorShopBussinessService;

namespace H5.SuperiorShop.Controllers
{
    [CheckLoginFitler]
    [JsonException] 
    public class UserController : BaseController
    {
        private readonly IUserService _IUserService;
        private readonly IShopAppService _IShopAppService;
        private readonly IMarketingService _IMarketingService;
        private readonly IOrderService _IOrderService;
        private readonly IQuestionService _IQuestionService;

        public UserController(IUserService IUserService, IShopAppService IShopAppService, IMarketingService IMarketingService, IOrderService IOrderService, IQuestionService IQuestionService)
        {
            _IUserService = IUserService;
            _IShopAppService = IShopAppService;
            _IMarketingService = IMarketingService;
            _IOrderService = IOrderService;
            _IQuestionService = IQuestionService;
        }
        protected override void Dispose(bool disposing)
        {
            this._IUserService.Dispose();
            this._IShopAppService.Dispose();
            this._IMarketingService.Dispose();
            this._IOrderService.Dispose();
            this._IQuestionService.Dispose();
            base.Dispose(disposing);
        }
        // GET: User

        /// <summary>
        /// 用户中心
        /// </summary>
        /// <param name="spid"></param>
        /// <returns></returns>
        /// 
        [CheckUrlFitler]
        [CheckShopStatus]
        public ActionResult Index(string spid)
        {
            var _spid = base.ValidataParms(spid);
            Func<ShopAppModel> fucc = delegate ()
            {
                return _IShopAppService.GetShopAppModel(_spid.ToInt());
            };
            var shopAppModel = DataIntegration.GetData<ShopAppModel>("GetShopAppModel_" + _spid, fucc);
            ViewBag.CountModel = _IOrderService.Api_GetOrderCountDs(UserContext.DeUserId.ToInt());
            return View(shopAppModel);
        }
        /// <summary>
        /// 优惠券列表
        /// </summary>
        /// <returns></returns>
        [CheckUrlFitler]
        public ActionResult CouponList()
        {
            string _userid = UserContext.DeUserId;
            int _said = base.CheckSaid();
            var myCoupons = _IMarketingService.Api_GetUserCouponList(_userid.ToInt());
            myCoupons = myCoupons.Where(p => p.IsExpress == false).ToList();
            var coupons = _IMarketingService.Api_GetCouponList(_said);
            var res = new Api_CouponManagerModel() {
                Coupons = coupons,
                MyCoupons = myCoupons
            };
            return View(res);
        }

     
        /// <summary>
        /// 领取优惠券
        /// </summary>
        /// <param name="couponid"></param>
        /// <param name="userid"></param>
        /// <returns></returns>
        /// 
        [AjaxOnly]
        public JsonResult GetCoupon(int couponid)
        {
            string _userid = UserContext.DeUserId;
            string msg = "";
            var ret = _IMarketingService.Api_GetCoupon(_userid.ToInt(), couponid, out msg);
            if (ret != 1)
                return Error(msg);
            return Success(true);
        }
        /// <summary>
        /// 提交投诉
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [AjaxOnly]
        [HttpPost]
        public JsonResult AddQuestion(QuestionParms model)
        {
            model.UserId = UserContext.DeUserId.ToInt();
            model.ShopAdminId = base.CheckSaid();
            model.ShopAppId = base.CheckSpid();
            _IQuestionService.Insert(model);
            return Success(model);
        }
    }
}