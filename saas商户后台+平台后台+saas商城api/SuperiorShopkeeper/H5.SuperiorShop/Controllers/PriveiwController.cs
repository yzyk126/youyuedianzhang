﻿using Redis;
using SuperiorModel;
using SuperiorShopBussinessService;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace H5.SuperiorShop.Controllers
{
    public class PriveiwController : BaseController
    {
        private readonly IProductService _IProductService;
        private readonly IShopAppService _IShopAppService;

        public PriveiwController(IProductService IProductService, IShopAppService IShopAppService)
        {
            _IProductService = IProductService;
            _IShopAppService = IShopAppService;

        }
        protected override void Dispose(bool disposing)
        {
            this._IProductService.Dispose();
            this._IShopAppService.Dispose();
            base.Dispose(disposing);
        }
        /// <summary>
        /// 给商户后台预览用
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            var parms = base.GetParms();
            if (parms.Spid == 0)
                return Error("非法参数");
            Func<Api_HomeDataModel> fucc = delegate ()
            {
                return _IProductService.Api_GetHomeData(parms.Spid);
            };
            var res = DataIntegration.GetData<Api_HomeDataModel>("GetHomeData_" + parms.Said, fucc, 1);
            //获取分类
            Func<List<Api_MenuItemModel>> fuccMenu = delegate ()
            {
                return _IProductService.Api_GetMenuList(parms.Said);
            };
            var menuList = DataIntegration.GetData<List<Api_MenuItemModel>>("GetMenuList_" + parms.Said, fuccMenu);
            ViewBag.MenuList = menuList;
            return View(res);
        }

        /// <summary>
        /// 获取首页专场
        /// </summary>
        /// <param name="said"></param>
        /// <returns></returns>
        [AjaxOnly]
        public JsonResult GetSpecialPlaceData(string said)
        {
            string _shopAdminId = base.ValidataParms(said);
            if (_shopAdminId == null)
                return Error("非法参数");
            //读取商品主体
            //读取商品主体
            Func<List<SpecialPlaceModel>> fucc = delegate ()
            {
                return _IProductService.SpecialPlaceList(_shopAdminId.ToInt());
            };
            var res = DataIntegration.GetData<List<SpecialPlaceModel>>("SpecialPlaceList" + _shopAdminId, fucc);
            return Success(res);
        }
    }
}