﻿using System.Linq;
using System.Web.Mvc;
using SuperiorCommon;
using SuperiorModel;
using SuperiorShopBussinessService;

namespace H5.SuperiorShop.Controllers
{
    [CheckLoginFitler]
    [JsonException]
    public class MarketingController : BaseController
    {
        private readonly IMarketingService _IMarketingService;

        public MarketingController(IMarketingService IMarketingService)
        {
            _IMarketingService = IMarketingService;

        }
        protected override void Dispose(bool disposing)
        {
            this._IMarketingService.Dispose();
            base.Dispose(disposing);
        }
        // GET: Marketing
        /// <summary>
        /// 获取当前用户得可用得优惠券列表
        /// </summary>
        /// <param name="userid"></param>
        /// <returns></returns>
        /// 
        [AjaxOnly]
        public JsonResult GetUserCoupon()
        {
            string _userid = UserContext.DeUserId;
            var res = _IMarketingService.Api_GetUserCouponList(_userid.ToInt());
            res = res.Where(p => p.IsExpress == false).ToList();
            return Success(res);
        }
        /// <summary>
        /// 获取当前商户未被删除得优惠券列表
        /// </summary>
        /// <param name="said"></param>
        /// <returns></returns>
        /// 
        [AjaxOnly]
        public JsonResult GetCouponList(string said)
        {
            string _said = base.ValidataParms(said);
            if (_said == null)
                return Error("参数异常");
            var res = _IMarketingService.Api_GetCouponList(_said.ToInt());
            return Success(res);
        }
        /// <summary>
        /// 领取优惠券
        /// </summary>
        /// <param name="couponid"></param>
        /// <param name="userid"></param>
        /// <returns></returns>
        /// 
        [AjaxOnly]
        public JsonResult GetCoupon(int couponid)
        {
            string _userid = UserContext.DeUserId;
            if (_userid == null)
                return Error("参数异常");
            string msg = "";
            var ret = _IMarketingService.Api_GetCoupon(_userid.ToInt(), couponid, out msg);
            if (ret != 1)
                return Error(msg);
            return Success(true);
        }
    }
}