﻿(function () {
    ProductDetailClass = {};
    var _CurrentProductModel;
    var _currentCombinePropetyIds = "";
    var _currentCombinePropetyNames = "";
    var _cartCount =parseInt($("#cartNum").html());
    var _currentBuyNum = 1;
    var said = ToolManager.Common.UrlParms("said");
    var spid = ToolManager.Common.UrlParms("spid");
    ProductDetailClass.Instance = {
        Init: function () {

            //轮播
            $(".swiper-zhutu").swiper({
                loop: true,
                paginationType: 'fraction',
                autoplay: 5000
            });
            $(function () {
                $(".Spinner").Spinner({ value: 1, len: 3, max: 999 });
                _CurrentProductModel = JSON.parse($("#hidden_pModel").val());
                if (_CurrentProductModel.CurrentSkuType == 2)
                    _currentCombinePropetyIds = ProductDetailClass.Instance.GetCurrentCombinePropetyIds();
            });
            $(window).scroll(function () {
                if ($(window).scrollTop() > 268) {
                    $("#tophovertree").fadeIn(100);
                }
                else {
                    $("#tophovertree").fadeOut(100);
                    //alert($(".Spinner input").val());
                }
            });
            $(document).on('click', '#btn_createOrder', this.GoToOrder);
            $(document).on('click', '#btn_saveCart', this.SaveCart);
            $(document).on('click', '.attr_propety', this.ChosePropety);
            //监听数量改变事件
            $(document).on('click', '.Decrease', this.NumChange);
            $(document).on('click', '.Increase', this.NumChange);
        },
        NumChange: function () {
          
            _currentBuyNum = $(".Amount").val();
            var choseStr = "";
            if (_CurrentProductModel.CurrentSkuType == 1) {
                choseStr = _CurrentProductModel.ProductName + "," + _currentBuyNum + _CurrentProductModel.Unit;
            }
            else {

                _currentCombinePropetyNames = ProductDetailClass.Instance.GetCurrentCombinePropetyNames();
                choseStr = _currentCombinePropetyNames + "," + _currentBuyNum + _CurrentProductModel.Unit;
               
            }
            $("#choseStr").html(choseStr);
        },
        //获取当前选择的SKU属性值组合
        GetCurrentCombinePropetyIds: function () {
            var res = [];
            $(".active").each(function () {
                res.push($(this).find("a").attr("propetyValueId"));
            })
            return res.join('-');
        },
        //获取当前选择的SKU属性名称组合
        GetCurrentCombinePropetyNames: function () {
            var res = [];
            $(".active").each(function () {
                res.push($(this).find("a").text());
            })
            return res.join('+');
        },
        //根据属性值组合ID获取SKU
        FindSkuModel: function (cps) {
            var res = null;
            for (var i = 0; i < _CurrentProductModel.SkuManagerModel.MoreSkuModelList.length; i++) {
                if (cps == _CurrentProductModel.SkuManagerModel.MoreSkuModelList[i].PropetyCombineId) {
                    res = _CurrentProductModel.SkuManagerModel.MoreSkuModelList[i];
                    break;
                }
            }
            return res;
        },
        ChosePropety: function () {
            $(this).parent().addClass("active").siblings().removeClass("active");
            _currentCombinePropetyIds = ProductDetailClass.Instance.GetCurrentCombinePropetyIds();
            _currentCombinePropetyNames = ProductDetailClass.Instance.GetCurrentCombinePropetyNames();
            var skuModel = ProductDetailClass.Instance.FindSkuModel(_currentCombinePropetyIds);
            $("#propety_price").html(skuModel.SallPrice + '<label class="fen"></label>');
            $("#propety_stockNum").html("库存 "+skuModel.StockNum + ' ' + _CurrentProductModel.Unit);
            $("#choseStr").html(_currentCombinePropetyNames + ',' + _currentBuyNum + _CurrentProductModel.Unit);
        },
        SaveCart: function () {
           
            var model = {};
            var _skuModel = null;
            model.ProductId = _CurrentProductModel.Id;
            model.SaleNum = _currentBuyNum;
            //验证库存和商品
            if (_CurrentProductModel.CurrentSkuType == 1) {
                _stockNum = _CurrentProductModel.SkuManagerModel.SingleSkuModel.StockNum;
                model.Product_SkuId = _CurrentProductModel.SkuManagerModel.SingleSkuModel.Id;
            }
            else {
                _skuModel = ProductDetailClass.Instance.FindSkuModel(_currentCombinePropetyIds);
                _stockNum = _skuModel.StockNum;
                model.Product_SkuId = _skuModel.Id;
            }
            if (_currentBuyNum > _stockNum) {
                layer.msg('库存不足');
                return false;
            }
         
            model.State = 1; //是否选择
            //从缓存取出列表
            $.get("/Cart/GetList?said=" + said + "&spid=" + spid + "&r=" + new Date().getTime(), function (data) {
                if (data.IsSuccess) {
                    //判断购物车中是否已经存在当前sku,有得话，更改数量，没有push
                    if (ShopCartManagerClass.Instance.IsExist(data.Data, model.Product_SkuId)) {
                        var skumodel = ShopCartManagerClass.Instance.GetModelBySkuId(data.Data, model.Product_SkuId)
                        skumodel.SaleNum = skumodel.SaleNum + parseInt(model.SaleNum);
                        data.Data = ShopCartManagerClass.Instance.UpdateSkuModel(data.Data, skumodel);
                        
                    } else {
                        data.Data.push(model);
                        _cartCount++;
                    }
                    //更新回
                    ProductDetailClass.Instance.SetCartList(data.Data);

                } else {
                    layer.msg(data.Message);
                }
            })
        },

        SetCartList:function(list){
            var index = layer.load(1);
            RequestManager.Ajax.Post("/Cart/Set?spid=" + spid+"&said="+said, list, true, function (data) {
                layer.close(index);
                
                if (data.IsSuccess) {
                    layer.msg("添加购物车成功!");
                    $("#cartNum").html(_cartCount);
                    $(".close-popup").click();
                } else {
                    layer.msg(data.Message);
                }
            })
        },

        GoToOrder: function () {
      
            var _stockNum = 0;
            var _skuModel = null;
            //验证库存
            if (_CurrentProductModel.CurrentSkuType == 1)
            {
                _stockNum = _CurrentProductModel.SkuManagerModel.SingleSkuModel.StockNum;
            }
            else
            {
                _skuModel = ProductDetailClass.Instance.FindSkuModel(_currentCombinePropetyIds);
                _stockNum =_skuModel.StockNum;
            }
            if (_currentBuyNum > _stockNum) {
                layer.msg('库存不足');
                return false;
            }
            var model = {};
            model.ProductId = _CurrentProductModel.Id;
            model.SaleNum = _currentBuyNum;
            var skuDetail = {};
            skuDetail.ProductId = _CurrentProductModel.Id;
            skuDetail.ProductName = _CurrentProductModel.ProductName;
            skuDetail.Brand = _CurrentProductModel.Brand;
            skuDetail.ImgPath = _CurrentProductModel.BannerImgs[0];
            skuDetail.ShippingTemplateId = _CurrentProductModel.ShipTemplateId;

            if (_CurrentProductModel.CurrentSkuType == 1) {
                model.Product_SkuId = _CurrentProductModel.SkuManagerModel.SingleSkuModel.Id;
                skuDetail.Product_SkuId = _CurrentProductModel.SkuManagerModel.SingleSkuModel.Id;
                skuDetail.SallPrice = _CurrentProductModel.SkuManagerModel.SingleSkuModel.SallPrice;
                skuDetail.StockNum = _CurrentProductModel.SkuManagerModel.SingleSkuModel.StockNum;
                skuDetail.ProppetyCombineName = "";
                skuDetail.SkuType = 1;
                skuDetail.Weight = _CurrentProductModel.SkuManagerModel.SingleSkuModel.Weight;   
            }
            else {
                model.Product_SkuId = _skuModel.Id;     
                skuDetail.Product_SkuId = _skuModel.Id;
                skuDetail.SallPrice = _skuModel.SallPrice;
                skuDetail.StockNum = _skuModel.StockNum;
                skuDetail.ProppetyCombineName = _skuModel.ProppetyCombineName;
                skuDetail.SkuType = 2;          
                skuDetail.Weight = _skuModel.Weight;     
            }
            model.SkuDetail = skuDetail;
            var sendModel = [];
            sendModel.push(model);
            window.location.href = "/Order/SureOrder?goodList=" + JSON.stringify(sendModel) + "&spid=" + spid + "&said=" + said+"&addresid=0";
        }
    }
})();