﻿using SuperiorCommon;
using SuperiorModel;
using System.Web;

namespace H5.SuperiorShop
{
    /// <summary>
    /// 当前系统登录对象
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class UserContext
    {
        /// <summary>
        /// 获取当前系统登录完整对象
        /// </summary>
        public static WxUserLoginResutModel User
        {
            get
            {
                var spid = HttpContext.Current.Request["spid"];
                var res = HttpContext.Current.Session[SessionKey.H5UserKey + spid] as WxUserLoginResutModel;
                LogManger.Instance.WriteLog("获取当前用户模型"+res.ToJson());
                return HttpContext.Current.Session[SessionKey.H5UserKey+spid] as WxUserLoginResutModel;
            }
        }
        /// <summary>
        /// 获取当前系统登录对象的解密ID
        /// </summary>
        public static string DeUserId
        {
            get
            {
                return SecretClass.DecryptQueryString(User.Id);
            }
        }
    }
}