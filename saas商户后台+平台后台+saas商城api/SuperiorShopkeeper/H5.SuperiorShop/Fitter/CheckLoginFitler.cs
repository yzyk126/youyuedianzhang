﻿using SuperiorCommon;
using System;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace H5.SuperiorShop
{
    /// <summary>
    /// 是否进行跳转登录过滤器
    /// 
    /// </summary>
    [AttributeUsage(AttributeTargets.All, AllowMultiple = true, Inherited = true)]
    public class CheckLoginFitler : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var spid = HttpContext.Current.Request["spid"];
            if (string.IsNullOrEmpty(spid))
            {
                filterContext.Result = new RedirectResult("/Error/Index?msg=" + HttpUtility.UrlEncode("请不要随意改动网站参数", Encoding.UTF8));
                return;
            }
            if (HttpContext.Current.Session[SessionKey.H5UserKey+spid] == null)
            {
                //记忆原地址url
                CookieHelper.SetCookie("LOGINJUMPURL", HttpContext.Current.Request.Url.ToString(), DateTime.Now.AddMinutes(20));    
                
                filterContext.Result = new RedirectResult("/Login/Login?spid=" + HttpContext.Current.Request["spid"]);
                return;
            }

        }
    }
}