﻿using Redis;
using SuperiorModel;
using SuperiorShopBussinessService;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace Api.WeiXinGroup.Controllers
{
    [ApiExceptionAttribute]
    public class ArticleController : ApiControllerBase
    {
        // GET: Article
        private readonly IArticleService _IArticleService;


        public ArticleController(IArticleService IArticleService)
        {
            _IArticleService = IArticleService;
        }
        protected override void Dispose(bool disposing)
        {
            this._IArticleService.Dispose();
            base.Dispose(disposing);
        }
        /// <summary>
        /// 查询资讯
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        [HttpPost]
        public ApiResultModel<object> GetArticleList(Api_ArticleListCriteria criteria)
        {
            var res = _IArticleService.Api_GetList(criteria.Offset, criteria.PageSize, criteria.Title);
            return Success(res);
        }
        /// <summary>
        /// 获取用户文章收藏列表
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        [HttpPost]
        public ApiResultModel<object> GetCollected(Api_GroupArticleCollectedListCriteria criteria)
        {
            var _userid = base.ValidataParms(criteria.UserId);
            if (_userid == null)
                return Error("非法参数");
            var res = _IArticleService.Api_GetCollectedList(criteria.Offset,criteria.PageSize,_userid.ToInt());
            return Success(res);
        }
        /// <summary>
        /// 获取资讯详情
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public ApiResultModel<object> GetArticle(int id)
        {
            Func<Api_ArticleDetailModel> func = () =>
            {
                return _IArticleService.Api_Get(id);
            };
            var res = DataIntegration.GetData<Api_ArticleDetailModel>("Group_Article_" + id, func);
            return Success(res);
        }
        /// <summary>
        /// 获取随机推荐文章
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// 
        [HttpGet]
        public ApiResultModel<object> GetRecommendList(int id)
        {
            Func<List<Api_ArticleItem>> func = () =>
            {
                return _IArticleService.Api_GetRecommendList(id);
            };
            var res = DataIntegration.GetData<List<Api_ArticleItem>>("Article_Recommend_" + id, func);
            return Success(res);
        }
        /// <summary>
        /// 更改资讯数据
        /// </summary>
        /// <param name="type">1阅读量，2分享量，3收藏量</param>
        /// <param name="id">资讯ID</param>
        /// <returns></returns>
        [HttpGet]
        public ApiResultModel<object> UpdateData(int type, int id)
        {
            _IArticleService.Api_UpdateData(type, id);
            return Success(true);
        }
        /// <summary>
        /// 获取轮播图
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ApiResultModel<object> BannerList(int pageType)
        {
            Func<List<Api_GroupBannerModel>> func = () =>
            {
                return _IArticleService.Api_GetGroupBannerList(pageType);
            };
            var res = DataIntegration.GetData<List<Api_GroupBannerModel>>("GroupBannerList_Page_"+ pageType, func);
            return Success(res);
        }
    }
}