﻿using SuperiorModel;

namespace SuperiorShopBussinessService
{
    public interface IDataService:IService
    {
        PagedSqlList<ShopOrderRankModel> SearchShopOrderRankDs(ShopOrderRankCriteria criteria);
        PagedSqlList<ShopUserRankModel> SearchShopUserRankDs(ShopUserRankCriteria criteria);
        PagedSqlList<ShopAppExpressRankModel> SearchShopAppExpressRankDs(ShopAppExpressRankCriteria criteria);
    }
}
