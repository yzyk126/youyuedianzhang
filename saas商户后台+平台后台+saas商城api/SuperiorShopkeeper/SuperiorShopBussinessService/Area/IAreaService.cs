﻿using SuperiorModel;
using System.Collections.Generic;

namespace SuperiorShopBussinessService
{
    public interface IAreaService:IService
    {
        List<AreaModel> Get(int parentId);
        List<AreaModel> GetAll();
    }
}
