﻿using System;
using System.Collections.Generic;
using System.Linq;
using SuperiorModel;
using SuperiorShopDataAccess;
using System.Data;

namespace SuperiorShopBussinessService
{
    public class HomeService : IHomeService
    {
        public void Dispose()
        {

        }

        public HomeModel GetHomeData(int shopAdminId)
        {
            var res = new HomeModel();
            var ds = HomeDataAccess.GetHomeData(shopAdminId);
            if (DataManager.CheckDs(ds, 6))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    res.TotalProductCount = DataManager.GetRowValue_Int(ds.Tables[0].Rows[0]["TotalProductCount"]);
                }
                if (DataManager.CheckHasRow(ds.Tables[1]))
                {
                    res.TotalUserCount = DataManager.GetRowValue_Int(ds.Tables[1].Rows[0]["TotalUserCount"]);
                }
                if (DataManager.CheckHasRow(ds.Tables[2]))
                {
                    res.TotalOrderCount = DataManager.GetRowValue_Int(ds.Tables[2].Rows[0]["TotalOrderCount"]);
                }
                if (DataManager.CheckHasRow(ds.Tables[3]))
                {
                    res.TotalOrderAmount = DataManager.GetRowValue_Decimal(ds.Tables[3].Rows[0]["TotalOrderAmount"]);
                }
                if (DataManager.CheckHasRow(ds.Tables[4]))
                {
                    res.WaitSendCount = DataManager.GetRowValue_Int(ds.Tables[4].Rows[0]["WaitSendCount"]);
                }
                if (DataManager.CheckHasRow(ds.Tables[5]))
                {
                    res.WaitServiceCount = DataManager.GetRowValue_Int(ds.Tables[5].Rows[0]["WaitServiceCount"]);
                }

            }
            return res;
        }

        public AdminHomeModel GetAdminHomeData()
        {
            var res = new AdminHomeModel();
            var ds = HomeDataAccess.GetAdminHomeData();
            if (DataManager.CheckDs(ds, 12))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    res.TotalProductCount = DataManager.GetRowValue_Int(ds.Tables[0].Rows[0]["TotalProductCount"]);
                }
                if (DataManager.CheckHasRow(ds.Tables[1]))
                {
                    res.TotalUserCount = DataManager.GetRowValue_Int(ds.Tables[1].Rows[0]["TotalUserCount"]);
                }
                if (DataManager.CheckHasRow(ds.Tables[2]))
                {
                    res.TotalOrderCount = DataManager.GetRowValue_Int(ds.Tables[2].Rows[0]["TotalOrderCount"]);
                }
                if (DataManager.CheckHasRow(ds.Tables[3]))
                {
                    res.TotalOrderAmount = DataManager.GetRowValue_Decimal(ds.Tables[3].Rows[0]["TotalOrderAmount"]);
                }
                if (DataManager.CheckHasRow(ds.Tables[4]))
                {
                    res.TotalShopAdminCount = DataManager.GetRowValue_Int(ds.Tables[4].Rows[0]["TotalShopAdminCount"]);
                }
                if (DataManager.CheckHasRow(ds.Tables[5]))
                {
                    res.TotalPayShopAdminCount = DataManager.GetRowValue_Int(ds.Tables[5].Rows[0]["TotalPayShopAdminCount"]);
                }
                if (DataManager.CheckHasRow(ds.Tables[6]))
                {
                    res.UsingShopAdminCount = DataManager.GetRowValue_Int(ds.Tables[6].Rows[0]["UsingShopAdminCount"]);
                }
                if (DataManager.CheckHasRow(ds.Tables[7]))
                {
                    res.UsingProgramCount = DataManager.GetRowValue_Int(ds.Tables[7].Rows[0]["UsingProgramCount"]);
                }
                if (DataManager.CheckHasRow(ds.Tables[8]))
                {
                    res.UsingH5Count = DataManager.GetRowValue_Int(ds.Tables[8].Rows[0]["UsingH5Count"]);
                }
                if (DataManager.CheckHasRow(ds.Tables[9]))
                {
                    res.TotalH5PayAmount = DataManager.GetRowValue_Decimal(ds.Tables[9].Rows[0]["TotalH5PayAmount"]);
                }
                if (DataManager.CheckHasRow(ds.Tables[10]))
                {
                    res.TotalProgramPayAmount = DataManager.GetRowValue_Decimal(ds.Tables[10].Rows[0]["TotalProgramPayAmount"]);
                }
                if (DataManager.CheckHasRow(ds.Tables[11]))
                {
                    res.TotalPayAmount = DataManager.GetRowValue_Decimal(ds.Tables[11].Rows[0]["TotalPayAmount"]);
                }

            }
            return res;
        }

        public Chart_AddUserModel GetAddUserChart(int shopAdminId)
        {
            var res = new Chart_AddUserModel();
            var tempList = new List<string>();
            var ds = HomeDataAccess.GetAddUserChart(shopAdminId);
            if (DataManager.CheckDs(ds, 1))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        tempList.Add(Convert.ToDateTime(dr["CreateTime"]).ToString("yyyy-MM-dd"));
                    }
                    //开始计算每日的用户增长量
                    foreach (var date in res.DateList)
                    {
                        var _count = tempList.Where(i => i == date).Count();
                        res.DataList.Add(_count);
                    }
                }
            }
            return res;
        }

        public Chart_TypeUserModel GetTypeUserChart(int shopAdminId)
        {
            var res = new Chart_TypeUserModel();
            var ds = HomeDataAccess.GetTypeUserChart(shopAdminId);
            if (DataManager.CheckDs(ds, 2))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    res.XcxCount = DataManager.GetRowValue_Int(ds.Tables[0].Rows[0]["XcxCount"]);
                }
                if (DataManager.CheckHasRow(ds.Tables[1]))
                {
                    res.H5Count = DataManager.GetRowValue_Int(ds.Tables[1].Rows[0]["H5Count"]);
                }
            }
            return res;
        }

        public Chart_SallModel GetSallChart(int shopAdminId)
        {
            var res = new Chart_SallModel();
            var ds = HomeDataAccess.GetSallChart(shopAdminId);
            if (DataManager.CheckDs(ds, 1))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    var _tempList = new List<Chart_SallItem>();
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        _tempList.Add(new Chart_SallItem() {
                            CreateTime = Convert.ToDateTime(dr["CreateTime"]).ToString("yyyy-MM-dd"),
                            TotalAmount = DataManager.GetRowValue_Decimal(dr["TotalAmount"]),
                            TotalCount = DataManager.GetRowValue_Int(dr["TotalCount"])
                        });
                    }
                    //开始计算图表需要的数据格式
                    res.DateList.ForEach((date) =>
                    {
                        var _item = _tempList.Where(i => i.CreateTime == date).FirstOrDefault();
                        //日期数组（近7日，已经排好序）
                        if (_item != null)
                        {
                            res.OrderAmountList.Add(_item.TotalAmount);
                            res.OrderCountList.Add(_item.TotalCount);
                        }
                        else
                        {
                            //给0
                            res.OrderAmountList.Add(0);
                            res.OrderCountList.Add(0);
                        }
                    });
                }
            }
            return res;
        }

        public CreateRechargeOrderResult CreateRechargeOrder(int shopAdminId, int rechargeId,out string msg)
        {
            var res = new CreateRechargeOrderResult();
            int ret = 1;
            var ds = HomeDataAccess.CreateRechargeOrder(shopAdminId, rechargeId,out ret);
            msg = "";
            if (ret != 1)
            {
                switch (ret)
                {
                    case 2:
                        msg = "充值产品不存在";
                        break;
                    case -1:
                        msg = "提交订单失败";
                        break;
                }
            }
            else
            {
                if (DataManager.CheckDs(ds, 1))
                {
                    if (DataManager.CheckHasRow(ds.Tables[0]))
                    {
                        DataRow dr = ds.Tables[0].Rows[0];
                        res.Id = Convert.ToInt32(dr["Id"]);
                        res.OrderNo = dr["RechargeNo"].ToString();
                        res.Amount = double.Parse(dr["Amount"].ToString());
                    }

                }
            }
           
            return res;
        }

        public int WxRechargeCall(string orderno, string payCode, int state, out string msg)
        {
            var ret = HomeDataAccess.WxRechargeCall(orderno, payCode,state);
            msg = "";
            switch (ret)
            {
                case 2:
                    msg = "订单不存在或已完成";
                    break;
                case 3:
                    msg = "事务出错";
                    break;
                case -1:
                    msg = "回调出错";
                    break;
            }
            return ret;
        }

        public int GetRechargeOrderStatus(int id)
        {
            var res = 0;
            var ds = HomeDataAccess.GetRechargeOrderStatus(id);
            if (DataManager.CheckDs(ds, 1))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    res = Convert.ToInt32(ds.Tables[0].Rows[0]["OrderStatus"]);
                }
            }
            return res;
        }



        public Chart_AddUserModel GetAdminAddUserChart()
        {
            var res = new Chart_AddUserModel();
            var tempList = new List<string>();
            var ds = HomeDataAccess.GetAdminAddUserChart();
            if (DataManager.CheckDs(ds, 1))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        tempList.Add(Convert.ToDateTime(dr["CreateTime"]).ToString("yyyy-MM-dd"));
                    }
                    //开始计算每日的用户增长量
                    foreach (var date in res.DateList)
                    {
                        var _count = tempList.Where(i => i == date).Count();
                        res.DataList.Add(_count);
                    }
                }
            }
            return res;
        }

        public Chart_TypeUserModel GetAdminTypeUserChart()
        {
            var res = new Chart_TypeUserModel();
            var ds = HomeDataAccess.GetAdminTypeUserChart();
            if (DataManager.CheckDs(ds, 2))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    res.XcxCount = DataManager.GetRowValue_Int(ds.Tables[0].Rows[0]["XcxCount"]);
                }
                if (DataManager.CheckHasRow(ds.Tables[1]))
                {
                    res.H5Count = DataManager.GetRowValue_Int(ds.Tables[1].Rows[0]["H5Count"]);
                }
            }
            return res;
        }

        public Chart_SallModel GetAdminSallChart()
        {
            var res = new Chart_SallModel();
            var ds = HomeDataAccess.GetAdminSallChart();
            if (DataManager.CheckDs(ds, 1))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    var _tempList = new List<Chart_SallItem>();
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        _tempList.Add(new Chart_SallItem()
                        {
                            CreateTime = Convert.ToDateTime(dr["CreateTime"]).ToString("yyyy-MM-dd"),
                            TotalAmount = DataManager.GetRowValue_Decimal(dr["TotalAmount"]),
                            TotalCount = DataManager.GetRowValue_Int(dr["TotalCount"])
                        });
                    }
                    //开始计算图表需要的数据格式
                    res.DateList.ForEach((date) =>
                    {
                        var _item = _tempList.Where(i => i.CreateTime == date).FirstOrDefault();
                        //日期数组（近7日，已经排好序）
                        if (_item != null)
                        {
                            res.OrderAmountList.Add(_item.TotalAmount);
                            res.OrderCountList.Add(_item.TotalCount);
                        }
                        else
                        {
                            //给0
                            res.OrderAmountList.Add(0);
                            res.OrderCountList.Add(0);
                        }
                    });
                }
            }
            return res;
        }
    }
}