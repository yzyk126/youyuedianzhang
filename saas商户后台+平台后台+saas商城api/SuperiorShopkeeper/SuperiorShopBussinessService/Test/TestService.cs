﻿using System;
using System.Collections.Generic;
using SuperiorModel;
using SuperiorShopDataAccess;
using System.Data;

namespace SuperiorShopBussinessService
{
    public class TestService : ITestService
    {
        public void Dispose()
        {
            
        }

        public List<TestModel> TestList()
        {
            var res = new List<TestModel>();
            var ds = TestDataAccess.TestList();
            if (DataManager.CheckDs(ds,1))
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    res.Add(new TestModel()
                    {
                        Id = Convert.ToInt32(dr["Id"]),
                        Name = dr["Name"].ToString(),
                        Age = Convert.ToInt32(dr["Age"])
                    });
                };
            }
            return res;
        }
    }
}
