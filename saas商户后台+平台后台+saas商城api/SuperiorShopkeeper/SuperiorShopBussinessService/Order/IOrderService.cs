﻿using SuperiorModel;
using System.Collections.Generic;

namespace SuperiorShopBussinessService
{
    public interface IOrderService:IService
    {
        PagedSqlList<OrderModel> Search(OrderCriteria criteria);
        OrderDetailModel GetDetail(int orderid);
        void SendProduct(SendProductParms model);
        void FinishService(int orderid);
        Api_WeixinCreateOrderModel Program_CreateOrder_Cart(Api_CreateOrderCriteria model, out string msg);
        int Program_WxNotice_Cart(string out_trade_no, int status, float total_fee, string transaction_id);
        Api_OrderDetail_InfoModel Api_GetDetail(int orderid);
        List<Api_OrderListModel> Api_GetOrderList(Api_OrderListCriteria criteria);
        int Api_GetOrderStatus(int orderId);
        bool Api_ServiceOrder(int orderid, int userid);
        Api_OrderCountModel Api_GetOrderCountDs(int userid);
        void UpdateDelivery(SendProductParms model);
    }
}
