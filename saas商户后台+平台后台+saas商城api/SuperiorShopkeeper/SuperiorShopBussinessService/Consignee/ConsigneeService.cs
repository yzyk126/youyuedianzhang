﻿using System;
using System.Collections.Generic;
using SuperiorModel;
using SuperiorShopDataAccess;
using System.Data;

namespace SuperiorShopBussinessService
{
    public class ConsigneeService : IConsigneeService
    {
        public int Add(Api_ConsigneeModel criteria, out string msg)
        {
            var ret = ConsigneeDataAccess.Add(criteria);
            msg = string.Empty;
            switch (ret)
            {
                case 2:
                    msg = "用户异常";
                    break;
                default:
                    msg = "请求异常";
                    break;
            }
            return ret;
        }

        public void Del(int id, int userid)
        {
            ConsigneeDataAccess.Del(id,userid);
        }

        public void Dispose()
        {
            
        }

        public void Edit(Api_ConsigneeModel criteria)
        {
            ConsigneeDataAccess.Edit(criteria);
        }

        public List<Api_ConsigneeModel> GetList(int userid)
        {
            var res = new List<Api_ConsigneeModel>();
            var ds = ConsigneeDataAccess.GetList(userid);
            if (DataManager.CheckDs(ds, 1))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        res.Add(new Api_ConsigneeModel()
                        {
                           Id = Convert.ToInt32(dr["Id"]),
                            IsDefault = Convert.ToInt32(dr["IsDefault"]),
                            ConsigneeName = dr["ConsigneeName"].ToString(),
                            ConsigneePhone = dr["ConsigneePhone"].ToString(),
                            ZipCode = dr["ZipCode"].ToString(),
                            ProvinceName = dr["ProvinceName"].ToString(),
                            CityName = dr["CityName"].ToString(),
                            AreaName = dr["AreaName"].ToString(),
                            ProvinceCode = dr["ProvinceCode"].ToString(),
                            CityCode = dr["CityCode"].ToString(),
                            AreaCode = dr["AreaCode"].ToString(),
                            Address = dr["Address"].ToString()
                        });
                    }
                }

            }
            return res;
        }

        public void UpdateIsDefault(int id, int userid, int isdefault)
        {
            ConsigneeDataAccess.UpdateIsDefault(id,userid,isdefault);
        }
    }
}
