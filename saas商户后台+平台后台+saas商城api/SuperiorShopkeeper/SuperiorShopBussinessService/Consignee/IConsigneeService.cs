﻿using System.Collections.Generic;
using SuperiorModel;

namespace SuperiorShopBussinessService
{
    public interface IConsigneeService:IService
    {
        List<Api_ConsigneeModel> GetList(int userid);
        void UpdateIsDefault(int id, int userid, int isdefault);
        void Del(int id, int userid);
        void Edit(Api_ConsigneeModel criteria);
        int Add(Api_ConsigneeModel criteria, out string msg);
    }
}
