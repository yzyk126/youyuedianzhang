﻿using System;
using System.Collections.Generic;
using SuperiorCommon;
using SuperiorModel;
using SuperiorShopDataAccess;
using System.Data;

namespace SuperiorShopBussinessService
{
    public class ManagerAccountService : IManagerAccountService
    {
        public void Dispose()
        {

        }

        public PagedSqlList<ManagerAccountModel> SearchManagerAccountList(ManagerAccountListCriteria criteria)
        {
            var queryList = new List<ManagerAccountModel>();
            var totalCount = 0;
            var ds = ManagerAccountDataAccess.SearchManagerAccountList(criteria);
            if (DataManager.CheckDs(ds, 2))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        queryList.Add(new ManagerAccountModel()
                        {
                            Id = dr["Id"].ToString(),
                            OpenId = dr["OpenId"].ToString(),
                            PhoneNumber = dr["PhoneNumber"].ToString(),
                            TrueName = dr["TrueName"].ToString(),
                            Amount = DataManager.GetRowValue_Decimal(dr["Amount"]),
                            NickName = dr["NickName"].ToString(),
                            OptionStatus = DataManager.GetRowValue_Int(dr["OptionStatus"]),
                            CreateTime = Convert.ToDateTime(dr["CreateTime"]),
                            HeadImgUrl = dr["HeadImgUrl"].ToString()
                        });
                    }
                }
                totalCount = Convert.ToInt32(ds.Tables[1].Rows[0]["totalCount"]);
            }
            return new PagedSqlList<ManagerAccountModel>(queryList, criteria.PagingResult.PageIndex, criteria.PagingResult.PageSize, totalCount);
        }

        public PagedSqlList<ManagerAccountOrderModel> SearchManagerAccountOrderList(ManagerAccountOrderListCriteria criteria)
        {
            var queryList = new List<ManagerAccountOrderModel>();
            var totalCount = 0;
            var ds = ManagerAccountDataAccess.SearchManagerAccountOrderList(criteria);
            if (DataManager.CheckDs(ds, 2))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        queryList.Add(new ManagerAccountOrderModel()
                        {
                            Id = Convert.ToInt32(dr["Id"]),
                            OrderNo = dr["OrderNo"].ToString(),
                            PayCode = dr["PayCode"].ToString(),
                            PayTime = DataManager.GetRowValue_DateTime(dr["PayTime"]),
                            OpenId = dr["OpenId"].ToString(),
                            PhoneNumber = dr["PhoneNumber"].ToString(),
                            TrueName = dr["TrueName"].ToString(),
                            Amount = DataManager.GetRowValue_Decimal(dr["Amount"]),
                            Remark = dr["Remark"].ToString(),
                            OrderStatus = DataManager.GetRowValue_Int(dr["OrderStatus"]),
                            NickName = dr["NickName"].ToString(),
                            CreateTime = Convert.ToDateTime(dr["CreateTime"]),
                            AccountManagerId = Convert.ToInt32(dr["AccountManagerId"])
                        });
                    }
                }
                totalCount = Convert.ToInt32(ds.Tables[1].Rows[0]["totalCount"]);
            }
            return new PagedSqlList<ManagerAccountOrderModel>(queryList, criteria.PagingResult.PageIndex, criteria.PagingResult.PageSize, totalCount);
        }

        public void OptionManagerAccount(int id, int option)
        {
            ManagerAccountDataAccess.OptionManagerAccount(id, option);
        }
        public int TransferAmount(int id, out string msg)
        {
            var ret = ManagerAccountDataAccess.TransferAmount(id);
            msg = "";
            switch (ret)
            {
                case 2:
                    msg = "订单已成功,请勿重复提交";
                    break;
                case 3:
                    msg = "账户余额不足";
                    break;
                case 4:
                    msg = "执行出错";
                    break;
            }
            return ret;
        }

        public void UpdateCashCoutOder(string orderno, int status)
        {
            ManagerAccountDataAccess.UpdateCashCoutOder(orderno, status);
        }
        public ManagerAccountModel Wx_Login(string openid, string headImgUrl, string nickName)
        {
            var res = new ManagerAccountModel();
            var ds = ManagerAccountDataAccess.Wx_Login(openid, headImgUrl, nickName);
            if (DataManager.CheckDs(ds, 1))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    DataRow dr = ds.Tables[0].Rows[0];
                    res.Id =SecretClass.EncryptQueryString(dr["Id"].ToString());
                    res.PhoneNumber = dr["PhoneNumber"].ToString();
                    res.TrueName = dr["TrueName"].ToString();
                    res.Amount = DataManager.GetRowValue_Decimal(dr["Amount"]);
                    res.NickName = dr["NickName"].ToString();
                    res.OptionStatus = DataManager.GetRowValue_Int(dr["OptionStatus"]);
                    res.CreateTime = Convert.ToDateTime(dr["CreateTime"]);
                    res.HeadImgUrl = dr["HeadImgUrl"].ToString();
                }
            }
            return res;
        }
        public ManagerAccountModel GetModel(int id)
        {
            var res = new ManagerAccountModel();
            var ds = ManagerAccountDataAccess.GetModel(id);
            if (DataManager.CheckDs(ds, 1))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    DataRow dr = ds.Tables[0].Rows[0];
                    res.Id = SecretClass.EncryptQueryString(dr["Id"].ToString());
                    res.PhoneNumber = dr["PhoneNumber"].ToString();
                    res.TrueName = dr["TrueName"].ToString();
                    res.Amount = DataManager.GetRowValue_Decimal(dr["Amount"]);
                    res.NickName = dr["NickName"].ToString();
                    res.OptionStatus = DataManager.GetRowValue_Int(dr["OptionStatus"]);
                    res.CreateTime = Convert.ToDateTime(dr["CreateTime"]);
                    res.HeadImgUrl = dr["HeadImgUrl"].ToString();
                }
            }
            return res;
        }

        public SubmitCashOutResultModel CreateCashOutOrder(int managerAccountId, decimal amount, string orderno, out string msg)
        {
            var ret = 0;
            var ds = ManagerAccountDataAccess.CreateCashOutOrder(managerAccountId, amount, orderno, out ret);
            msg = "";
            var res = new SubmitCashOutResultModel();
            if (ret != 1)
            {
                switch (ret)
                {
                    case 2:
                        msg = "账号已被冻结";
                        break;
                    case 3:
                        msg = "账户余额不足";
                        break;
                    case 4:
                        msg = "参数异常";
                        break;
                    case 5:
                        msg = "执行出错";
                        break;
                    default:
                        msg = "执行异常";
                        break;
                }
            }
            else
            {
                if (DataManager.CheckDs(ds, 1))
                {
                    if (DataManager.CheckHasRow(ds.Tables[0]))
                    {
                        var dr = ds.Tables[0].Rows[0];
                        res.ExpressAmount = dr["ExpressAmount"].ToString();
                        res.OpenId = dr["OpenId"].ToString();
                    }
                }
            }

            return res;
        }

        public int GetProceedsCount(int id)
        {
            var res = 0;
            var ds = ManagerAccountDataAccess.GetProceedsCount(id);
            if (DataManager.CheckDs(ds, 1))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    var dr = ds.Tables[0].Rows[0];
                    res = Convert.ToInt32(dr["total"]);
                }
            }
            return res;
        }

        public List<ServiceCashOutLogItem> SearchServiceCashOutLogItemList(int managerAccountId, int pageIndex, int pageSize)
        {
            var res = new List<ServiceCashOutLogItem>();
            var ds = ManagerAccountDataAccess.SearchServiceCashOutLogItemDs(managerAccountId, pageIndex, pageSize);
            if (DataManager.CheckDs(ds, 1))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        res.Add(new ServiceCashOutLogItem()
                        {
                            OrderStatus = Convert.ToInt32(dr["OrderStatus"]),
                            OrderNo = dr["OrderNo"].ToString(),
                            CreateTime = dr["CreateTime"].ToString(),                     
                            Amount = DataManager.GetRowValue_Decimal(dr["Amount"]),
                        });
                    }
                }
            }
            return res;
        }
        public List<ServiceProceedsItem> SearchServiceProceedsItemList(int managerAccountId, int pageIndex, int pageSize)
        {
            var res = new List<ServiceProceedsItem>();
            var ds = ManagerAccountDataAccess.SearchServiceProceedsItemDs(managerAccountId, pageIndex, pageSize);
            if (DataManager.CheckDs(ds, 1))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        res.Add(new ServiceProceedsItem()
                        {
                            AppType = Convert.ToInt32(dr["AppType"]),
                            LoginName = dr["LoginName"].ToString().Substring(0,4)+"******",
                            Title = dr["Title"].ToString(),
                            CreateTime = dr["CreateTime"].ToString(),
                            RechargeAmount = DataManager.GetRowValue_Decimal(dr["Amount"]),
                            AccountMangerAmount = DataManager.GetRowValue_Decimal(dr["AccountMangerAmount"])
                        });
                    }
                }
            }
            return res;
        }
    }
}
