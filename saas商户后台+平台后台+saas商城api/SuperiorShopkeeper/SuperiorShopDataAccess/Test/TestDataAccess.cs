﻿using SuperiorSqlTools;
using System.Data;

namespace SuperiorShopDataAccess
{
    public class TestDataAccess
    {
        public static DataSet TestList()
        {
            var sqlManager = new SqlManager();
            //SqlParameter[] parms =
            //{
            //    new SqlParameter("@shopId",shopId)
            //};
            var sql = "select * from C_Test";
            return sqlManager.ExecuteDataset(CommandType.Text,sql , null);
        }
    }
}
