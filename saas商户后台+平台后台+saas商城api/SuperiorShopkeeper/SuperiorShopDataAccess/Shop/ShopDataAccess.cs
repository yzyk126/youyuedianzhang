﻿using SuperiorSqlTools;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using SuperiorModel;

namespace SuperiorShopDataAccess
{
    public class ShopDataAccess
    {
        public static DataSet GetShopModel(string username, string password)
        {
            var sqlManager = new SqlManager();
            SqlParameter[] parms =
            {
                new SqlParameter("@username",username),
                new SqlParameter("@password",password)
            };
            var sql = "select * from C_ShopAdmin where LoginName=@username and PassWord=@password";
            return sqlManager.ExecuteDataset(CommandType.Text, sql, parms);
        }

        public static DataSet GetShopAppIdsByShopAdminId(int shopAdminId)
        {
            var sqlManager = new SqlManager();
            SqlParameter[] parms =
            {
                new SqlParameter("@shopAdminId",shopAdminId)
            };
            var sql = "seleCt Id from C_ShopApp where ShopAdminId=@shopAdminId";
            return sqlManager.ExecuteDataset(CommandType.Text, sql, parms);
        }

        public static DataSet GetRechargeList()
        {
            var sqlManager = new SqlManager();
            var sql = "select * from dbo.C_Recharge";
            return sqlManager.ExecuteDataset(CommandType.Text, sql, null);
        }

        public static DataSet GetShopModel_Role(string username, string password)
        {
            var sqlManager = new SqlManager();
            SqlParameter[] parms =
            {
                new SqlParameter("@username",username),
                new SqlParameter("@password",password)
            };
            var sql = "select sra.*,sa.*,sr.RoleName,sr.Authoritys,sra.OptionStatus as RoleOptionStatus,sa.OptionStatus as AdminOptionStatus,sa.Id as ShopAdminId  from C_ShopRoleAccount sra left join C_ShopRole sr on sra.RoleId=sr.Id left join C_ShopAdmin sa on sra.ShopAdminId=sa.Id where sra.Account=@username and sra.PassWord=@password and sra.IsDel=0";
            return sqlManager.ExecuteDataset(CommandType.Text, sql, parms);
        }

        public static int InsertShop(ShopInfoModel model)
        {
            var sqlManager = new SqlManager();
            SqlParameter rtn_err = sqlManager.GetRtnParameter();
            SqlParameter[] parms =
            {
                new SqlParameter("@LoginName", model.LoginName),
                new SqlParameter("@PassWord",model.PassWord),
                new SqlParameter("@Introduce",model.Introduce),
                new SqlParameter("@PhoneNumber",model.PhoneNumber),
                new SqlParameter("@qqCode",model.QqCode==null?"":model.QqCode),//为什么不会自动转化曹
                new SqlParameter("@WxCode",model.WxCode==null?"":model.WxCode),
                new SqlParameter("@Contact",model.Contact),
                new SqlParameter("@AccountManagerId",model.AccountManagerId),
                rtn_err
            };
            sqlManager.ExecuteNonQuery(CommandType.StoredProcedure, "InsertShop", parms);
            if (rtn_err.Value != null)
            {
                return int.Parse(rtn_err.Value.ToString());
            }
            return -1;
        }

        public static DataSet ShopCashOutAmount(int shopAdminId,decimal amount,string  orderno,out int ret)
        {
            ret = 0;
            var sqlManager = new SqlManager();
            SqlParameter rtn_err = sqlManager.GetRtnParameter();
            SqlParameter[] parms =
            {
                new SqlParameter("@shopAdminId", shopAdminId),
                new SqlParameter("@orderno",orderno),
                new SqlParameter("@amount",amount),
                rtn_err
            };
            var res = sqlManager.ExecuteDataset(CommandType.StoredProcedure, "create_shop_cashout_order", parms);
            if (rtn_err.Value != null)
            {
                ret =  int.Parse(rtn_err.Value.ToString());
            }
            return res;
        }

        public static void UpdateShopCashCoutOder(string orderno, int status)
        {
            var sqlManager = new SqlManager();
            SqlParameter[] parms =
            {
                new SqlParameter("@orderno", orderno),
                new SqlParameter("@status",status)
            };
            string sql = "";
            if (status == 1)
            {
                sql = "update C_ShopCashOutOrder set OrderStatus = @status ,PayTime=GETDATE()  where OrderNo = @orderno and OrderStatus = 0";
            }
            else
            {
                sql = "update C_ShopCashOutOrder set OrderStatus = @status where OrderNo = @orderno and OrderStatus = 0";
            }
             
            sqlManager.ExecuteNonQuery(CommandType.Text, sql, parms);
        }

        public static void UpdateShopAdmin(string openid, string nickname, string headImgUrl, int id)
        {
            var sqlManager = new SqlManager();
            SqlParameter[] parms =
            {
                new SqlParameter("@openid", openid),
                new SqlParameter("@nickname",nickname),
                new SqlParameter("@headImgUrl",headImgUrl),
                new SqlParameter("@id",id)
               
            };
            string sql = "update C_ShopAdmin set OpenId = @openid,HeadImgUrl=@headImgUrl,NickName=@nickname where Id= @id";
            sqlManager.ExecuteNonQuery(CommandType.Text, sql, parms);
        }

        public static DataSet SearchShopCashOutDs(ShopCashOutCriteria criteria)
        {
            var sqlManager = new SqlManager();
            List<SqlParameter> parms = new List<SqlParameter>();
            var sb = sqlManager.CreateSb();
            if (!string.IsNullOrEmpty(criteria.LoginName))
            {
                sb.Append(" and LoginName=@LoginName");
                parms.Add(new SqlParameter("@LoginName", criteria.LoginName));
            }
            if (criteria.OrderStatus != 999)
            {
                sb.Append(" and OrderStatus=@OrderStatus ");
                parms.Add(new SqlParameter("@OrderStatus", criteria.OrderStatus));
            }
            
            var sql = string.Format("select top({0}) * from(select ROW_NUMBER() over(order by Id desc)as rownum,* from C_ShopCashOutOrder  where {1} )tt where tt.rownum>{2};select count(*) as totalCount from C_ShopCashOutOrder sa where {1}", criteria.PagingResult.PageSize, sb.ToString(), criteria.PagingResult.PageSize * criteria.PagingResult.PageIndex);
            return sqlManager.ExecuteDataset(CommandType.Text, sql, parms.ToArray());
        }

    }
}
