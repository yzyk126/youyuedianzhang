﻿using System.Collections.Generic;
using System.Linq;
using SuperiorModel;
using SuperiorSqlTools;
using System.Data;
using System.Data.SqlClient;

namespace SuperiorShopDataAccess
{
    public class ManagerAccountDataAccess
    {
        public static DataSet SearchManagerAccountList(ManagerAccountListCriteria criteria)
        {
            var sqlManager = new SqlManager();
            List<SqlParameter> parms = new List<SqlParameter>();
            var sb = sqlManager.CreateSb();
            if (!string.IsNullOrEmpty(criteria.PhoneNumber))
            {
                sb.Append(" and PhoneNumber=@PhoneNumber");
                parms.Add(new SqlParameter("@PhoneNumber", criteria.PhoneNumber));
            }
            if (!string.IsNullOrEmpty(criteria.TrueName))
            {
                sb.Append(" and TrueName=@TrueName ");
                parms.Add(new SqlParameter("@TrueName", criteria.TrueName));
            }
            if (criteria.OptionStatus != 999)
            {
                sb.Append(" and OptionStatus=@OptionStatus ");
                parms.Add(new SqlParameter("@OptionStatus", criteria.OptionStatus));
            }
           
            var sql = string.Format("select top({0}) * from(select ROW_NUMBER() over(order by Id desc)as rownum,* from C_ManagerAccount  where {1} )tt where tt.rownum>{2};select count(*) as totalCount from C_ManagerAccount  where {1} ", criteria.PagingResult.PageSize, sb.ToString(), criteria.PagingResult.PageSize * criteria.PagingResult.PageIndex);
            return sqlManager.ExecuteDataset(CommandType.Text, sql, parms.ToArray());
        }

        public static DataSet SearchManagerAccountOrderList(ManagerAccountOrderListCriteria criteria)
        {
            var sqlManager = new SqlManager();
            List<SqlParameter> parms = new List<SqlParameter>();
            var sb = sqlManager.CreateSb();
            if (!string.IsNullOrEmpty(criteria.PhoneNumber))
            {
                sb.Append(" and m.PhoneNumber=@PhoneNumber");
                parms.Add(new SqlParameter("@PhoneNumber", criteria.PhoneNumber));
            }
            if (!string.IsNullOrEmpty(criteria.TrueName))
            {
                sb.Append(" and m.TrueName=@TrueName ");
                parms.Add(new SqlParameter("@TrueName", criteria.TrueName));
            }
            if (!string.IsNullOrEmpty(criteria.OrderNo))
            {
                sb.Append(" and o.OrderNo=@OrderNo ");
                parms.Add(new SqlParameter("@OrderNo", criteria.OrderNo));
            }
            if (criteria.OrderStatus != 999)
            {
                sb.Append(" and o.OrderStatus=@OrderStatus ");
                parms.Add(new SqlParameter("@OrderStatus", criteria.OrderStatus));
            }

            var sql = string.Format("select top({0}) * from(select ROW_NUMBER() over(order by o.Id desc)as rownum,o.*,m.TrueName,m.OpenId,m.PhoneNumber from C_AccountMangerOrderLog o left join C_ManagerAccount m on o.AccountManagerId=m.Id  where {1} )tt where tt.rownum>{2};select count(*) as totalCount from C_AccountMangerOrderLog o left join C_ManagerAccount m on o.AccountManagerId=m.Id  where {1} ", criteria.PagingResult.PageSize, sb.ToString(), criteria.PagingResult.PageSize * criteria.PagingResult.PageIndex);
            return sqlManager.ExecuteDataset(CommandType.Text, sql, parms.ToArray());
        }
         
        public static void OptionManagerAccount(int id, int option)
        {
            var sqlManager = new SqlManager();
            SqlParameter[] parms =
            {
                new SqlParameter("@id",id),
                new SqlParameter("@option",option)
            };
            var sql = "update C_ManagerAccount set OptionStatus=@option where Id=@id";
            sqlManager.ExecuteNonQuery(CommandType.Text, sql, parms);
        }

        public static DataSet Wx_Login(string openid, string headImgUrl, string nickName)
        {
            var sqlmanager = new SqlManager();
            SqlParameter[] parms =
            {
                new SqlParameter("@openid",openid),
                new SqlParameter("@nickName",nickName),
                new SqlParameter("@headImgUrl",headImgUrl)
            };
            return sqlmanager.ExecuteDataset(CommandType.StoredProcedure, "wx_login_mannager_account", parms);
        }

        public static DataSet GetModel(int id)
        {
            var sqlmanager = new SqlManager();
            SqlParameter[] parms =
            {
                new SqlParameter("@id",id)
            };
            var sql = "select * from C_ManagerAccount where Id=@id";
            return sqlmanager.ExecuteDataset(CommandType.Text, sql, parms);
        }

        public static void UpdateCashCoutOder(string orderno, int status)
        {
            var sqlManager = new SqlManager();
            SqlParameter[] parms =
            {
                new SqlParameter("@orderno", orderno),
                new SqlParameter("@status",status)
            };
            string sql = "";
            if (status == 1)
            {
                sql = "update C_AccountMangerOrderLog set OrderStatus = @status ,PayTime=GETDATE()  where OrderNo = @orderno and OrderStatus = 0";
            }
            else
            {
                sql = "update C_AccountMangerOrderLog set OrderStatus = @status where OrderNo = @orderno and OrderStatus = 0";
            }

            sqlManager.ExecuteNonQuery(CommandType.Text, sql, parms);
        }

        public static DataSet CreateCashOutOrder(int managerAccountId, decimal amount, string orderno, out int ret)
        {
            ret = 0;
            var sqlManager = new SqlManager();
            SqlParameter rtn_err = sqlManager.GetRtnParameter();
            SqlParameter[] parms =
            {
                new SqlParameter("@managerAccountId", managerAccountId),
                new SqlParameter("@orderno",orderno),
                new SqlParameter("@amount",amount),
                rtn_err
            };
            var res = sqlManager.ExecuteDataset(CommandType.StoredProcedure, "create_manageraccount_cashorder", parms);
            if (rtn_err.Value != null)
            {
                ret = int.Parse(rtn_err.Value.ToString());
            }
            return res;
        }

        public static int TransferAmount(int id)
        { 
            var sqlManager = new SqlManager();
            SqlParameter rtn_err = sqlManager.GetRtnParameter();
            SqlParameter[] parms =
            {
                new SqlParameter("@id",id),
                rtn_err
            };
            sqlManager.ExecuteNonQuery(CommandType.StoredProcedure ,"transferAmount_managerAccount", parms);
            if (rtn_err.Value != null)
            {
                return int.Parse(rtn_err.Value.ToString());

            }
            return -1;
        }

        public static DataSet GetProceedsCount(int id)
        {
            var sqlManager = new SqlManager();
            SqlParameter[] parms =
           {
                new SqlParameter("@id",id)
            };
            var sql = "select Count(Id) as total from C_RechargeOrder where AccountManagerId = @id";
            return sqlManager.ExecuteDataset(CommandType.Text,sql,parms);
        }

        public static DataSet SearchServiceCashOutLogItemDs(int managerAccountId,int pageIndex,int pageSize)
        {
            var sqlManager = new SqlManager();
            SqlParameter[] parms =
           {
                new SqlParameter("@managerAccountId",managerAccountId)
            };
            var sql = string.Format("select top({0}) * from (select ROW_NUMBER() over(order by Id desc) as num,* from C_AccountMangerOrderLog where AccountManagerId=@managerAccountId) t where t.num>{1}", pageSize,pageSize*pageIndex);
            return sqlManager.ExecuteDataset(CommandType.Text, sql, parms.ToArray());
        }

        public static DataSet SearchServiceProceedsItemDs(int managerAccountId, int pageIndex, int pageSize)
        {
            var sqlManager = new SqlManager();
            SqlParameter[] parms =
           {
                new SqlParameter("@managerAccountId",managerAccountId)
            };
            var sql = string.Format("select top({0}) * from (select ROW_NUMBER() over(order by Id desc) as num,LoginName,Amount,Title,AppType,CreateTime,AccountMangerAmount from C_RechargeOrder where AccountManagerId=@managerAccountId) t where t.num>{1}", pageSize, pageSize * pageIndex);
            return sqlManager.ExecuteDataset(CommandType.Text, sql, parms.ToArray());
        }
    }
}
