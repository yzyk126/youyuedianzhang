﻿using System.Collections.Generic;
using SuperiorModel;
using SuperiorSqlTools;
using System.Data;
using System.Data.SqlClient;

namespace SuperiorShopDataAccess
{
    public class ShopAdminDataAccess
    {
        public static DataSet GetShopAdminModel(int id)
        {
            var sqlManager = new SqlManager();
            SqlParameter[] parms =
            {
                new SqlParameter("@id",id)
            };
            var sql = "select * from C_ShopAdmin where Id=@id";
            return sqlManager.ExecuteDataset(CommandType.Text, sql, parms);
        }

        public static DataSet SearchRechareOrderDs(RechareOrderCriteria criteria)
        {
            var sqlManager = new SqlManager();
            List<SqlParameter> parms = new List<SqlParameter>();
            var sb = sqlManager.CreateSb();
            if (!string.IsNullOrEmpty(criteria.LoginName))
            {
                sb.Append(" and LoginName=@LoginName");
                parms.Add(new SqlParameter("@LoginName", criteria.LoginName));
            }
            if (!string.IsNullOrEmpty(criteria.RechargeNo))
            {
                sb.Append(" and RechargeNo=@RechargeNo");
                parms.Add(new SqlParameter("@RechargeNo", criteria.RechargeNo));
            }
            if (criteria.ShopAdminId != 0)
            {
                sb.Append(" and ShopAdminId=@ShopAdminId ");
                parms.Add(new SqlParameter("@ShopAdminId", criteria.ShopAdminId));
            }
            if (!string.IsNullOrEmpty(criteria.AccountManagerId))
            {
                sb.Append(" and AccountManagerId=@AccountManagerId ");
                parms.Add(new SqlParameter("@AccountManagerId", criteria.AccountManagerId));
            }
            if (criteria.OrderStatus != 999)
            {
                sb.Append(" and OrderStatus=@OrderStatus ");
                parms.Add(new SqlParameter("@OrderStatus", criteria.OrderStatus));
            }
            if (criteria.AppType != 999)
            {
                sb.Append(" and AppType=@AppType ");
                parms.Add(new SqlParameter("@AppType", criteria.AppType));
            }
            if (criteria.SallType != 999)
            {
                sb.Append(" and SallType=@SallType ");
                parms.Add(new SqlParameter("@SallType", criteria.SallType));
            }

            if (!string.IsNullOrEmpty(criteria.BeginTime))
            {
                sb.Append(" and CreateTime>=@BeginTime ");
                parms.Add(new SqlParameter("@BeginTime", criteria.BeginTime));
            }
            if (!string.IsNullOrEmpty(criteria.EndTime))
            {
                sb.Append(" and CreateTime<=@EndTime ");
                parms.Add(new SqlParameter("@EndTime", criteria.EndTime));
            }

            var sql = string.Format("select top({0}) * from(select ROW_NUMBER() over(order by Id desc)as rownum,*  from C_RechargeOrder  where {1} )tt where tt.rownum>{2};select count(*) as totalCount from C_RechargeOrder where {1}", criteria.PagingResult.PageSize, sb.ToString(), criteria.PagingResult.PageSize * criteria.PagingResult.PageIndex);
            return sqlManager.ExecuteDataset(CommandType.Text, sql, parms.ToArray());
        }

        public static DataSet GetShopAdminListDs(ShopAdminCriteria criteria)
        {
            var sqlManager = new SqlManager();
            List<SqlParameter> parms = new List<SqlParameter>();
            var sb = sqlManager.CreateSb();
            if (!string.IsNullOrEmpty(criteria.PhoneNumber))
            {
                sb.Append(" and PhoneNumber=@PhoneNumber");
                parms.Add(new SqlParameter("@PhoneNumber", criteria.PhoneNumber));
            }
            if (criteria.OptionStatus != 999)
            {
                sb.Append(" and OptionStatus=@OptionStatus ");
                parms.Add(new SqlParameter("@OptionStatus", criteria.OptionStatus));
            }
            if (!string.IsNullOrEmpty(criteria.LoginName))
            {
                sb.Append(" and LoginName=@LoginName ");
                parms.Add(new SqlParameter("@LoginName", criteria.LoginName));
            }
            if (!string.IsNullOrEmpty(criteria.AccountManagerId))
            {
                sb.Append(" and AccountManagerId=@AccountManagerId ");
                parms.Add(new SqlParameter("@AccountManagerId", criteria.AccountManagerId));
            }

            var sql = string.Format("select top({0}) * from(select ROW_NUMBER() over(order by Id desc)as rownum,*  from C_ShopAdmin  where {1} )tt where tt.rownum>{2};select count(*) as totalCount from C_ShopAdmin where {1}", criteria.PagingResult.PageSize, sb.ToString(), criteria.PagingResult.PageSize * criteria.PagingResult.PageIndex);
            return sqlManager.ExecuteDataset(CommandType.Text, sql, parms.ToArray());
        }
        public static void OptionShopAdminStatus(int id, int option)
        {
            var sqlManager = new SqlManager();
            SqlParameter[] parms =
          {
                 new SqlParameter("@id", id),
                 new SqlParameter("@option", option)
            };
            var sql = "update C_ShopAdmin set OptionStatus=@option where Id=@id";
            sqlManager.ExecuteNonQuery(CommandType.Text, sql, parms);
        }
        public static void EditShopAdmin(ShopInfoModel model)
        {
            var sqlManager = new SqlManager();
            SqlParameter[] parms =
            {
                new SqlParameter("@id",model.Id),
                new SqlParameter("@Introduce",model.Introduce),
                new SqlParameter("@QqCode",model.QqCode.IsNull()),
                new SqlParameter("@WxCode",model.WxCode.IsNull()),
                new SqlParameter("@Contact",model.Contact.IsNull())
            };
            var sql = "update C_ShopAdmin set Introduce=@Introduce,QqCode=@QqCode,WxCode=@WxCode,Contact=@Contact where Id=@id";
            sqlManager.ExecuteNonQuery(CommandType.Text, sql, parms);
        }
        public static DataSet GetAdminModel(string loginName, string passWord)
        {
            var sqlManager = new SqlManager();
            SqlParameter[] parms =
            {
                new SqlParameter("@loginName",loginName),
                new SqlParameter("@passWord",passWord)
            };
            var sql = "select * from C_Admin where LoginName=@loginName and PassWord=@passWord";
            return sqlManager.ExecuteDataset(CommandType.Text, sql, parms);
        }

        public static void EditPw(int shopAdminId, string pw)
        {
            var sqlManager = new SqlManager();
            SqlParameter[] parms =
            {
                new SqlParameter("@shopAdminId",shopAdminId),
                new SqlParameter("@pw",pw)
            };
            var sql = "update C_ShopAdmin set PassWord=@pw where Id=@shopAdminId";
            sqlManager.ExecuteNonQuery(CommandType.Text, sql, parms);
        }

        public static DataSet GetRoleListDs(int shopAdminId)
        {
            var sqlManager = new SqlManager();
            SqlParameter[] parms =
            {
                new SqlParameter("@shopAdminId",shopAdminId)
            };
            var sql = "select * from C_ShopRole where ShopAdminId=@shopAdminId and IsDel=0 order by Id desc";
            return sqlManager.ExecuteDataset(CommandType.Text, sql, parms);
        }

        public static DataSet GetRoleAccountListDs(int shopAdminId)
        {
            var sqlManager = new SqlManager();
            SqlParameter[] parms =
            {
                new SqlParameter("@shopAdminId",shopAdminId)
            };
            var sql = "select sra.*,sr.RoleName from C_ShopRoleAccount sra left join C_ShopRole sr on sra.RoleId=sr.Id where sra.ShopAdminId=@shopAdminId and sra.IsDel=0 order by sra.Id desc";
            return sqlManager.ExecuteDataset(CommandType.Text, sql, parms);
        }

        public static DataSet GetRoleDs(int id)
        {
            var sqlManager = new SqlManager();
            SqlParameter[] parms =
            {
                new SqlParameter("@id",id)
            };
            var sql = "select * from C_ShopRole where Id=@id and IsDel=0";
            return sqlManager.ExecuteDataset(CommandType.Text, sql, parms);
        }
        public static DataSet GetRoleAccountDs(int id)
        {
            var sqlManager = new SqlManager();
            SqlParameter[] parms =
            {
                new SqlParameter("@id",id)
            };
            var sql = "select * from C_ShopRoleAccount where Id=@id and IsDel=0";
            return sqlManager.ExecuteDataset(CommandType.Text, sql, parms);
        }

        public static int AddShopRole(ShopRoleModel model)
        {
            var sqlManager = new SqlManager();
            SqlParameter rtn_err = sqlManager.GetRtnParameter();
            SqlParameter[] parms =
            {
                new SqlParameter("@RoleName", model.RoleName),
                new SqlParameter("@ShopAdminId", model.ShopAdminId),
                rtn_err
            };
            sqlManager.ExecuteNonQuery(CommandType.StoredProcedure, "[AddShopRole]", parms);
            if (rtn_err.Value != null)
            {
                return int.Parse(rtn_err.Value.ToString());
            }
            return -1;
        }

        public static int AddShopRoleAccount(ShopRoleAccountModel model)
        {
            var sqlManager = new SqlManager();
            SqlParameter rtn_err = sqlManager.GetRtnParameter();
            SqlParameter[] parms =
            {
                new SqlParameter("@Account", model.Account),
                new SqlParameter("@ShopAdminId", model.ShopAdminId),
                new SqlParameter("@PassWord", model.PassWord),
                new SqlParameter("@RoleId", model.RoleId),
                rtn_err
            };
            sqlManager.ExecuteNonQuery(CommandType.StoredProcedure, "[AddShopRoleAccount]", parms);
            if (rtn_err.Value != null)
            {
                return int.Parse(rtn_err.Value.ToString());
            }
            return -1;
        }
        public static int EditShopRoleAccount(ShopRoleAccountModel model)
        {
            var sqlManager = new SqlManager();
            SqlParameter rtn_err = sqlManager.GetRtnParameter();
            SqlParameter[] parms =
            {
                new SqlParameter("@Account", model.Account),
                new SqlParameter("@Id", model.Id),
                new SqlParameter("@PassWord", model.PassWord),
                new SqlParameter("@RoleId", model.RoleId),
                rtn_err
            };
            sqlManager.ExecuteNonQuery(CommandType.StoredProcedure, "[EditShopRoleAccount]", parms);
            if (rtn_err.Value != null)
            {
                return int.Parse(rtn_err.Value.ToString());
            }
            return -1;
        }

        public static int EditShopRole(ShopRoleModel model)
        {
            var sqlManager = new SqlManager();
            SqlParameter rtn_err = sqlManager.GetRtnParameter();
            SqlParameter[] parms =
            {
                 new SqlParameter("@Id", model.Id),
                new SqlParameter("@RoleName", model.RoleName),
                rtn_err
            };
            sqlManager.ExecuteNonQuery(CommandType.StoredProcedure, "[EditShopRole]", parms);
            if (rtn_err.Value != null)
            {
                return int.Parse(rtn_err.Value.ToString());
            }
            return -1;
        }
        public static int DelShopRole(int id)
        {
            var sqlManager = new SqlManager();
            SqlParameter rtn_err = sqlManager.GetRtnParameter();
            SqlParameter[] parms =
            {
                 new SqlParameter("@Id", id),
                rtn_err
            };
            sqlManager.ExecuteNonQuery(CommandType.StoredProcedure, "[DelShopRole]", parms);
            if (rtn_err.Value != null)
            {
                return int.Parse(rtn_err.Value.ToString());
            }
            return -1;
        }

        public static void DelShopRoleAccount(int id)
        {
            var sqlManager = new SqlManager();
            SqlParameter[] parms =
          {
                 new SqlParameter("@id", id)
            };
            var sql = "update C_ShopRoleAccount set IsDel=1 where Id=@id";
            sqlManager.ExecuteNonQuery(CommandType.Text, sql, parms);
        }
        public static void OptionShopRoleAccount(int id,int option)
        {
            var sqlManager = new SqlManager();
            SqlParameter[] parms =
          {
                 new SqlParameter("@id", id),
                 new SqlParameter("@option", option)
            };
            var sql = "update C_ShopRoleAccount set OptionStatus=@option where Id=@id";
            sqlManager.ExecuteNonQuery(CommandType.Text, sql, parms);
        }

        public static void SetShopRoleAuthority(ShopRoleModel model)
        {
            var sqlManager = new SqlManager();
            SqlParameter[] parms =
          {
                 new SqlParameter("@Id", model.Id),
                 new SqlParameter("@Authoritys", model.Authoritys)
            };
            var sql = "update C_ShopRole set Authoritys=@Authoritys where Id=@Id";
            sqlManager.ExecuteNonQuery(CommandType.Text,sql,parms);
        }


    }
}
