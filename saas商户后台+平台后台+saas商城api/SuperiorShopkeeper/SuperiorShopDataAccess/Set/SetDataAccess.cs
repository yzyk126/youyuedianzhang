﻿using System;
using System.Collections.Generic;
using SuperiorModel;
using SuperiorSqlTools;
using System.Data;
using System.Data.SqlClient;

namespace SuperiorShopDataAccess
{
    public class SetDataAccess
    {
        public static DataSet GetShippingTemplateList(int shopAdminId)
        {
            var sqlManager = new SqlManager();
            SqlParameter[] parms =
            {
                new SqlParameter("@shopAdminId",shopAdminId)
            };
            var sql = "select * from C_ShippingTemplates where ShopAdminId=@shopAdminId and IsDel=0";
            return sqlManager.ExecuteDataset(CommandType.Text, sql, parms);
        }

        public static int DelShippingTemplate(int id)
        {
            var sqlManager = new SqlManager();
            SqlParameter rtn_err = sqlManager.GetRtnParameter();
            SqlParameter[] parms =
            {
                new SqlParameter("@id", id),
                rtn_err
            };
            sqlManager.ExecuteNonQuery(CommandType.StoredProcedure, "[DelShippingTemplate]", parms);
            if (rtn_err.Value != null)
            {
                return int.Parse(rtn_err.Value.ToString());
            }
            return -1;
        }

        public static DataSet GetShippingTemplateModel(int id)
        {
            var sqlManager = new SqlManager();
            SqlParameter[] parms =
            {
                new SqlParameter("@id",id)
            };
            var sql = "select * from C_ShippingTemplates where Id=@id;select* from C_ShippingTemplateItem where ShippingTemplateId = @id and IsDel=0";
            return sqlManager.ExecuteDataset(CommandType.Text, sql, parms);
        }

        public static bool EditShippingTemplate(ShippingTemplateModel model)
        {
            var res = true;
            var sqlManager = new SqlManager();
            SqlParameter[] parms =
            {
                new SqlParameter("@ShippingTemplatesName", model.ShippingTemplatesName),
                new SqlParameter("@ChargType",model.ChargType),
                new SqlParameter("@Id",model.Id),
                new SqlParameter("@ShopAdminId", model.ShopAdminId)

            };

            var sqlList = new List<string>();
            sqlList.Add(string.Format("update C_ShippingTemplates set ShippingTemplatesName=@ShippingTemplatesName,ChargType=@ChargType where Id=@Id and ShopAdminId=@ShopAdminId"));
            sqlList.Add(string.Format("update C_ShippingTemplateItem set IsDel=1 where ShippingTemplateId=@Id and IsDel=0"));
            model.Items.ForEach((item) =>
            {
                sqlList.Add(string.Format("insert into C_ShippingTemplateItem(ShippingTemplateId,ProvinceIds,FirstValue,FirstAmount,NextValue,NextAmount,ProvinceStr)values({0},'{1}',{2},{3},{4},{5},'{6}')", model.Id, item.ProvinceIds, item.FirstValue, item.FirstAmount, item.NextValue, item.NextAmount, item.ProvinceStr));
            });
            return sqlManager.ExecTransactionAsUpdate_Public(sqlList.ToArray(), parms);
        }

        public static bool AddShippingTemplate(ShippingTemplateModel model)
        {
            var res = true;
            var sqlManager = new SqlManager();
            SqlParameter[] parms =
            {
                new SqlParameter("@ShippingTemplatesName", model.ShippingTemplatesName),
                new SqlParameter("@ChargType",model.ChargType),
                new SqlParameter("@ShopAdminId", model.ShopAdminId)

            };
            var p_sql = "insert into C_ShippingTemplates(ShippingTemplatesName,ChargType,ShopAdminId)Values(@ShippingTemplatesName,@ChargType,@ShopAdminId);select @@IDENTITY";
            object o = sqlManager.ExecuteScalar(p_sql, parms);
            if (o != null && o != DBNull.Value)
            {
                var newid = Convert.ToInt32(o);
                //--开始插入运费模板子项
                var sqlList = new List<string>();
                model.Items.ForEach((item) =>
                {
                    sqlList.Add(string.Format("insert into C_ShippingTemplateItem(ShippingTemplateId,ProvinceIds,FirstValue,FirstAmount,NextValue,NextAmount,ProvinceStr)values({0},'{1}',{2},{3},{4},{5},'{6}')", newid, item.ProvinceIds, item.FirstValue, item.FirstAmount, item.NextValue, item.NextAmount, item.ProvinceStr));
                });
                var r = sqlManager.ExecTransactionAsUpdate(sqlList.ToArray(), null);
                //如果事务失败，将模板删除
                if (!r)
                {
                    res = false;
                    var del_sql = "update C_ShippingTemplates set IsDel=1 where Id=" + newid;
                    sqlManager.ExecuteNonQuery(CommandType.Text, del_sql, null);
                }
            }
            else
            {
                res = false;
            }
            return res;

        }
    }
}
