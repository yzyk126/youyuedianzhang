﻿using System;

namespace SuperiorModel
{
    class UserModel
    {
    }

    public class UserListItem
    {
        public string Id { get; set; }
        public int ShopAppId { get; set; }
        public string ShopName { get; set; }
        public int AppType { get; set; }
        public string NickName { get; set; }
        public string HeadImgUrl { get; set; }
        public DateTime CreateTime { get; set; }
        public int OptionStatus { get; set; }
        public string LoginName { get; set; }

    }
}
