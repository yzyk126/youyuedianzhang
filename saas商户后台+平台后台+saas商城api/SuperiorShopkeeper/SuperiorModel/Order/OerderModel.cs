﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SuperiorModel
{
    public class OrderModel
    {
        public string Id { get; set; }
        public string OrderNo { get; set; }
        public string ShopName { get; set; }
        public string NickName { get; set; }
        public decimal RealityAmount { get; set; }
        public string PayOderNo { get; set; }
        public int AppType { get; set; }
        /// <summary>
        /// 0，待付款，1，（已发货）订单成功，2订单失败（15分钟未支付），3已付款，4申请售后订单，
        /// </summary>
        public int OrderStatus { get; set; }
        /// <summary>
        /// 售后订单处理状态，1已处理，0未处理
        /// </summary>
        public int ServiceStatus { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime PayTime { get; set; }
        public DateTime SendTime { get; set; }
        public string FailReason { get; set; }
        public string LoginName { get; set; }

    }

    public class OrderDetailModel
    {
        public string Id { get; set; }
        public OrderDetailModel()
        {
            this.OrderItems = new List<OrderItem>();
        }
        /// <summary>
        /// 订单编号
        /// </summary>
        public string OrderNo { get; set; }
        /// <summary>
        /// 收货人
        /// </summary>
        public string ConsigneeName { get; set; }
        /// <summary>
        /// 收货人电话
        /// </summary>
        public string ConsigneePhone { get; set; }
        /// <summary>
        /// 邮编
        /// </summary>
        public string ZipCode { get; set; }
        /// <summary>
        /// 收获地址
        /// </summary>
        public string Address { get; set; }
        /// <summary>
        /// 支付订单号
        /// </summary>
        public string PayOderNo { get; set; }
        /// <summary>
        /// 用户实际支付金额
        /// </summary>
        public string RealityAmount { get; set; }
        /// <summary>
        /// 订单状态0，待付款，1，（已发货）订单成功（sendstatus也为1），2订单失败（15分钟未支付），3已付款，4申请售后订单
        /// </summary>
        public int OrderStatus { get; set; }
        /// <summary>
        /// 发货状态，不参与条件查询，只做辅助判断
        /// </summary>
        public int SendStatus { get; set; }

        public DateTime CreateTime { get; set; }
        public string PayTime { get; set; }
        public string SendTime { get; set; }
        public string Remark { get; set; }
        public string FailReason { get; set; }
        /// <summary>
        /// 售后服务完成时间
        /// </summary>
        public string ServiceTime { get; set; }
        /// <summary>
        /// 售后服务状态。0未完成，1已完成
        /// </summary>
        public int ServiceStatus { get; set; }
        /// <summary>
        /// 订单商品金额
        /// </summary>
        public decimal OrderAmount { get; set; }
        /// <summary>
        /// 如果使用优惠券，优惠券的ID
        /// </summary>
        public int CouponId { get; set; }
        /// <summary>
        /// /优惠券名称
        /// </summary>
        public string CouponName { get; set; }
        /// <summary>
        /// 优惠了多少金额
        /// </summary>
        public decimal CouponDelAmount { get; set; }
        /// <summary>
        /// 运费
        /// </summary>
        public decimal ShippingAmount { get; set; }
        /// <summary>
        /// 快递名称
        /// </summary>
        public string DeliveryName { get; set; }
        /// <summary>
        /// 快递单号
        /// </summary>
        public string DeliveryNo { get; set; }


        public List<OrderItem> OrderItems { get; set; }
    }

    public class OrderItem
    {
        public string ProductName { get; set; }
        public int BuyNum { get; set; }
        public decimal SalePrice { get; set; }
        public string ProppetyCombineName { get; set; }
        public string SkuCode { get; set; }

    }

    public class SendProductParms
    {
        /// <summary>
        /// 订单ID加密
        /// </summary>
        /// 
        [Required(ErrorMessage = "参数非法")]
        public string Id { get; set; }
        [Required(ErrorMessage = "快递名称不能为空")]
        public string DeliveryName { get; set; }
        [Required(ErrorMessage = "快递单号不能为空")]
        public string DeliveryNo { get; set; }  
    }
}
