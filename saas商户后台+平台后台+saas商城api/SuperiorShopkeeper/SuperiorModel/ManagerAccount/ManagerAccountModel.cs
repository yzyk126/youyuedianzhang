﻿using System;

namespace SuperiorModel
{
    [Serializable]
    public class ManagerAccountModel
    {
        public string Id { get; set; }
        public string OpenId { get; set; }
        public string PhoneNumber { get; set; }
        public string TrueName { get; set; }
        public decimal Amount { get; set; }
        public int OptionStatus { get; set; }
        public DateTime CreateTime { get; set; }
        public string HeadImgUrl { get; set; }
        public string NickName { get; set; }
    }

    public class ManagerAccountOrderModel
    {
        public int Id { get; set; }
        public string OrderNo { get; set; }
        public string PayCode { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime PayTime { get; set; }
        public int OrderStatus { get; set; }
        public string OpenId { get; set; }
        public string PhoneNumber { get; set; }
        public string TrueName { get; set; }
        public decimal Amount { get; set; }
        public int AccountManagerId { get; set; }
        public string Remark { get; set; }
        public string  NickName { get; set; }

    }

    public class ServiceCashOutLogItem
    {
        public string OrderNo { get; set; }
        public int OrderStatus { get; set; }
        public string CreateTime { get; set; }
        public decimal Amount { get; set; }
    }

    public class ServiceProceedsItem
    {
        public string LoginName { get; set; }
        public int AppType { get; set; }
        public string Title { get; set; }
        public decimal AccountMangerAmount { get; set; }
        public string CreateTime { get; set; }
        public decimal RechargeAmount { get; set; }
    }
}
