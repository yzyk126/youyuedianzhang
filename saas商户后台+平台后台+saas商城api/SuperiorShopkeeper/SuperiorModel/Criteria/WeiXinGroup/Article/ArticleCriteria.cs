﻿namespace SuperiorModel
{
    public class ArticleCriteria : PagedBaseModel
    {
        public string Title { get; set; }

    }
}
