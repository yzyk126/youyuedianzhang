﻿namespace SuperiorModel
{
    public class ShopAdminCriteria:PagedBaseModel
    {
        public string PhoneNumber { get; set; }
        public string LoginName { get; set; }
        /// <summary>
        /// /店铺状态1正常，2冻结，-1封号,0审核中
        /// </summary>
        public int OptionStatus { get; set; }
        public string AccountManagerId { get; set; }

    }

    public class ShopCashOutCriteria:PagedBaseModel
    {
        public int OrderStatus { get; set; }
        public string LoginName { get; set; }
    }
}
