﻿namespace SuperiorModel
{
    public class AdminMenuCriteria:PagedBaseModel
    {
        public string MenuName { get; set; }
        public int OptionStatus { get; set; }
        public string LoginName { get; set; }
    }
}
