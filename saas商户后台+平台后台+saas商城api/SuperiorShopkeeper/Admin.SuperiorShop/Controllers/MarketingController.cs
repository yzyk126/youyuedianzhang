﻿using System.Web.Mvc;
using SuperiorModel;
using SuperiorShopBussinessService;

namespace Admin.SuperiorShop.Controllers
{
    [JsonException]
    [CheckLoginFitler]
    public class MarketingController : BaseController
    {
        private readonly IMarketingService _IMarketingService;
        private readonly IProductService _IProductService;
        private readonly IAreaService _IAreaService;

        public MarketingController(IMarketingService IMarketingService, IProductService IProductService, IAreaService IAreaService)
        {
            _IMarketingService = IMarketingService;
            _IProductService = IProductService;
            _IAreaService = IAreaService;
        }
        protected override void Dispose(bool disposin)
        {
            this._IMarketingService.Dispose();
            this._IProductService.Dispose();
            this._IAreaService.Dispose();
            base.Dispose(disposin);
        }
        // GET: Marketing
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 商户优惠券列表
        /// </summary>
        /// <returns></returns>
        public ActionResult CouponList()
        {
            var criteria = new AdminCouponCriteria() { CouponType = 999, PagingResult = new PagingResult(0, 20) };
            var model = GetCouponPagedModel(criteria);
            return View(model);
        }

        /// <summary>
        /// 搜索商户you'hui'quan'lie'biao
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        [AjaxOnly]
        [HttpPost]
        public ActionResult CouponList(AdminCouponCriteria criteria)
        {
            var model = GetCouponPagedModel(criteria);
            return PartialView("_CouponList",model);
        }
        private TPagedModelList<CouponModel> GetCouponPagedModel(AdminCouponCriteria criteria)
        {
            var query = _IMarketingService.SearchAdminCoupon(criteria);
            return new TPagedModelList<CouponModel>(query, query.PagingResult);
        }

    }
}