﻿using SuperiorCommon;
using SuperiorModel;
using SuperiorShopBussinessService;
using System;
using System.Web.Mvc;

namespace Admin.SuperiorShop.Controllers
{
    [JsonException]
    public class LoginController : BaseController
    {
        private readonly IShopAdminService _IShopAdminService;

        public LoginController(IShopAdminService IShopAdminService)
        {
            _IShopAdminService = IShopAdminService;
        }
        protected override void Dispose(bool disposin)
        {
            this._IShopAdminService.Dispose();
            base.Dispose(disposin);
        }
        // GET: Login
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Login()
        {
            return View();
        }
        /// <summary>
        /// 登录验证码
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        public FileContentResult CaptchaImage()
        {
            var captcha = new LiteralCaptcha(115, 48, 4);
            var bytes = captcha.Generate();
            Session["captcha"] = captcha.Captcha;
            return new FileContentResult(bytes, "image/jpeg");
        }

        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [AjaxOnly]
        [HttpPost]
        public JsonResult Login(ShopLoginParams model)
        {
            if (!ModelState.IsValid)
                return Error(base.ErrorMsg());
            var captcha = Session["captcha"];
            if (captcha == null)
                return Error("验证码错误");
            string strcaptcha = Convert.ToString(captcha);
            if (strcaptcha != model.Code)
                return Error("验证码错误");
            //查询店铺数据
            var shop = _IShopAdminService.GetAdminModel(model.UserName, MD5Manager.MD5Encrypt(model.PassWord));
            if (string.IsNullOrEmpty(shop.Id))
                return Error("账号或密码错误");
            shop.ShopAdminType = 1;
            SessionManager.SetSession(SessionKey.AdminnKey, shop);
            return Success(true);
        }

        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [AjaxOnly]
        [HttpPost]
        public JsonResult Login_Role(ShopLoginParams model)
        {
            if (!ModelState.IsValid)
                return Error(base.ErrorMsg());
            var captcha = Session["captcha"];
            if (captcha == null)
                return Error("验证码错误");
            string strcaptcha = Convert.ToString(captcha);
            if (strcaptcha != model.Code)
                return Error("验证码错误");
            //查询店铺数据
            //var shop = _IShopAdminService.GetShopModel_Role(model.UserName, MD5Manager.MD5Encrypt(model.PassWord));
            //if (string.IsNullOrEmpty(shop.Id))
            //    return Error("账号或密码错误");
            //if (shop.OptionStatus == 2)
            //    return Error("该店主账号已被冻结");
            //if (shop.OptionStatus == 0)
            //    return Error("账号审核中");
            //if (shop.ShopLoginRoleModel.OptionStatus == -1)
            //    return Error("当前员工账号被冻结");
            //SessionManager.SetSession(SessionKey.ShopKey, shop);
            return Error("");
        }
        /// <summary>
        /// 退出
        /// </summary>
        /// <returns></returns>
        [AjaxOnly]
        [HttpGet]
        public ActionResult Exist()
        {
            this.Session.Clear();
            return Success(true);
        }
    }
}