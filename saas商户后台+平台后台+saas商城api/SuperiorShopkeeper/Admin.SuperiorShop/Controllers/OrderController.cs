﻿using SuperiorShopBussinessService;
using System.Web.Mvc;
using SuperiorModel;

namespace Admin.SuperiorShop.Controllers
{
    [JsonException]
    [CheckLoginFitler]
    public class OrderController : BaseController
    {
        private readonly IOrderService _IOrderService;
        private readonly IShopAppService _IShopAppService;
        private readonly ICommentService _ICommentService;

        public OrderController(IOrderService IOrderService, IShopAppService IShopAppService, ICommentService ICommentService)
        {
            _IOrderService = IOrderService;
            _IShopAppService = IShopAppService;
            _ICommentService = ICommentService;
        }
        protected override void Dispose(bool disposin)
        {
            this._IOrderService.Dispose();
            this._IShopAppService.Dispose();
            this._ICommentService.Dispose();
            base.Dispose(disposin);
        }
        /// <summary>
        /// 订单列表页面
        /// </summary>
        /// <returns></returns>
        public ActionResult List()
        {
            var criteria = new OrderCriteria() { AppType = 999,  PagingResult = new PagingResult(0, 30), OrderStatus = 999 };
            var model = GetOrderPagedList(criteria);
            return View(model);
        }

        private TPagedModelList<OrderModel> GetOrderPagedList(OrderCriteria criteria)
        {
            criteria.ShopAppId = "unChecke";
            var query = _IOrderService.Search(criteria);
            return new TPagedModelList<OrderModel>(query, query.PagingResult);
        }
        /// <summary>
        ///订单列表搜索请求
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        [AjaxOnly]
        [HttpPost]
        public ActionResult List(OrderCriteria criteria)
        {
            var model = GetOrderPagedList(criteria);
            return PartialView("_List", model);
        }
        /// <summary>
        /// 详情页面
        /// </summary>
        /// <param name="orderid"></param>
        /// <returns></returns>
        public ActionResult Detail(string orderid)
        {
            int id = base.ValidataParms(orderid).ToInt();
            var model = _IOrderService.GetDetail(id);
            return View(model);
        }
        // GET: Order
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 评论列表
        /// </summary>
        /// <returns></returns>
        public ActionResult CommentList()
        {
            var criteria = new SearchCommentCriteria()
            {
                PagingResult = new PagingResult(0, 20)
            };
            var res = GetCommentPagedManager(criteria);
            return View(res);
        }
        /// <summary>
        /// 搜索评价 
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        [AjaxOnly]
        [HttpPost]
        public ActionResult CommentList(SearchCommentCriteria criteria)
        {
            var model = GetCommentPagedManager(criteria);
            return PartialView("_CommentList", model);
        }
        private TPagedModelList<CommentModel> GetCommentPagedManager(SearchCommentCriteria criteria)
        {
            var query = _ICommentService.Search(criteria);
            return new TPagedModelList<CommentModel>(query, query.PagingResult);
        }
        /// <summary>
        /// 删除评论
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [AjaxOnly]
        public JsonResult DelComment(int id)
        {
            _ICommentService.Del(id);
            return Success(true);
        }
    }
}