﻿using SuperiorCommon;
using SuperiorModel;
using System.Web;

namespace Admin.SuperiorShop
{
    /// <summary>
    /// 当前系统登录对象
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class UserContext
    {
        /// <summary>
        /// 获取当前系统登录完整对象
        /// </summary>
        public static AdminInfoModel User
        {
            get
            {
                return HttpContext.Current.Session[SessionKey.AdminnKey] as AdminInfoModel;
            }
        }
        /// <summary>
        /// 获取当前系统登录对象的解密ID
        /// </summary>
        public static string DeSecretId
        {
            get
            {
                return SecretClass.DecryptQueryString(User.Id);
            }
        }
    }
}