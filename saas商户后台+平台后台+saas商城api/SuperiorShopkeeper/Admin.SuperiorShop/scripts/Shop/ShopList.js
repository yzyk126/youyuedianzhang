﻿(function () {
    ShopListClass = {};
    var _searchCriteria = { PagingResult: { PageIndex: 0, PageSize: 20 }, OptionStatus:999 };
    ShopListClass.Instance = {
        Init: function () {
            //分页事件注册
            superior.ui.control.Pager.enablePaging(document, ShopListClass.Instance.Refresh);
            $(document).on('click', '#btn_search', this.Search);
            $(document).on('click', '.OptionShop', this.OptionStatus);
        },
        OptionStatus: function () {
            var id = $(this).attr("OptionId");
            var opt = $(this).attr("Opt");
            var index = layer.load(1);
            $.get("/Shop/OptionShop?id=" + id + "&optionStatus=" + opt + "&r=" + new Date().getTime(), function (data) {
                layer.close(index);
                if (data.IsSuccess) {
                    layer.alert("执行成功", function () {
                        window.location.href = "/Shop/ShopList";
                    })

                } else {
                    layer.alert(data.Message);
                }
            })
        },
       
        Search: function () {

            _searchCriteria.PhoneNumber = $("#search_PhoneNumber").val();
            _searchCriteria.LoginName = $("#search_LoginName").val();
            _searchCriteria.AccountManagerId = $.trim($("#search_AccountManagerId").val());
            _searchCriteria.OptionStatus = $("#search_status").val();
            ShopListClass.Instance.Refresh(0);
        },
        Refresh: function (pageIndex) {
            var url = "/Shop/ShopList";
            if (pageIndex !== undefined)
                _searchCriteria.PagingResult.PageIndex = pageIndex;
            var index = layer.load(1);
            RequestManager.Ajax.Post("/Shop/ShopList", _searchCriteria, true, function (data) {
                layer.close(index);
                $("#dataList").html(data);
            })
        }

    };

})();