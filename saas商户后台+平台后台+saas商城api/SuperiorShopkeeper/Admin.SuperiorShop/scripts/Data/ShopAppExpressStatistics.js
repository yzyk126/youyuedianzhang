﻿(function () {
    ShopAppExpressStatisticsClass = {};
    var _searchCriteria = { PagingResult: { PageIndex: 0, PageSize: 20 } };
    ShopAppExpressStatisticsClass.Instance = {
        Init: function () {
            //分页事件注册
            superior.ui.control.Pager.enablePaging(document, ShopAppExpressStatisticsClass.Instance.Refresh);
            $(document).on('click', '#btn_search', this.Search);
        },

        Search: function () {
            _searchCriteria.LoginName = $("#search_LoginName").val();
            _searchCriteria.AppType = $("#search_AppType").val()
            ShopAppExpressStatisticsClass.Instance.Refresh(0);
        },
        Refresh: function (pageIndex) {
            if (pageIndex !== undefined)
                _searchCriteria.PagingResult.PageIndex = pageIndex;
            var index = layer.load(1);
            RequestManager.Ajax.Post("/Data/ShopAppExpressStatistics", _searchCriteria, true, function (data) {
                layer.close(index);
                $("#dataList").html(data);
            })
        }

    };

})();